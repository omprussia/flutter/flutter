// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'package:meta/meta.dart';

import '../../doctor_validator.dart';
import '../../features.dart';

/// The aurora-devices-specific implementation of a [Workflow].
///
/// Will apply to the host platform / be able to launch & list devices only if
/// the aurora devices feature is enabled in the featureFlags argument.
///
/// Can't list emulators at all.
@immutable
class AuroraDeviceWorkflow implements Workflow {
  const AuroraDeviceWorkflow({required FeatureFlags featureFlags}) : _featureFlags = featureFlags;

  final FeatureFlags _featureFlags;

  @override
  bool get appliesToHostPlatform => _featureFlags.areAuroraDevicesEnabled;

  @override
  bool get canLaunchDevices => _featureFlags.areAuroraDevicesEnabled;

  @override
  bool get canListDevices => _featureFlags.areAuroraDevicesEnabled;

  @override
  bool get canListEmulators => false;
}
