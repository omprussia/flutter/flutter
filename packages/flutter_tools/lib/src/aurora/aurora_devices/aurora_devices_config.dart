// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'package:meta/meta.dart';

import '../../base/config.dart';
import '../../base/file_system.dart';
import '../../base/logger.dart';
import '../../base/platform.dart';
import '../../cache.dart';
import 'aurora_device_config.dart';

/// Represents the aurora devices config file on disk which in turn
/// contains a list of individual aurora device configs.
class AuroraDevicesConfig {
  /// Load a [AuroraDevicesConfig] from a (possibly non-existent) location on disk.
  ///
  /// The config is loaded on construction. Any error while loading will be logged
  /// but will not result in an exception being thrown. The file will not be deleted
  /// when it's not valid JSON (which other configurations do) and will not
  /// be implicitly created when it doesn't exist.
  AuroraDevicesConfig({
    required Platform platform,
    required FileSystem fileSystem,
    required Logger logger,
  })  : _platform = platform,
        _fileSystem = fileSystem,
        _logger = logger,
        _configLoader = (() => Config.managed(
              _kAuroraDevicesConfigName,
              fileSystem: fileSystem,
              logger: logger,
              platform: platform,
            ));

  @visibleForTesting
  AuroraDevicesConfig.test({
    required FileSystem fileSystem,
    required Logger logger,
    Directory? directory,
    Platform? platform,
  })  : _platform = platform ?? FakePlatform(),
        _fileSystem = fileSystem,
        _logger = logger,
        _configLoader =
            (() => Config.test(name: _kAuroraDevicesConfigName, directory: directory, logger: logger, managed: true));

  static const String _kAuroraDevicesConfigName = 'aurora_devices.json';
  static const String _kAuroraDevicesConfigKey = 'aurora-devices';
  static const String _kSchema = r'$schema';
  static const String _kAuroraDevices = 'aurora-devices';

  final Platform _platform;
  final FileSystem _fileSystem;
  final Logger _logger;
  final Config Function() _configLoader;

  // When the aurora devices feature is disabled, AuroraDevicesConfig is
  // constructed anyway. So loading the config in the constructor isn't a good
  // idea. (The Config ctor logs any errors)
  //
  // I also didn't want to introduce a FeatureFlags argument to the constructor
  // and conditionally load the config when the feature is enabled, because
  // sometimes we need that Config object even when the feature is disabled.
  // For example inside ensureFileExists, which is used when enabling
  // the feature.
  //
  // Instead, users of this config should handle the feature flags. So for
  // example don't get [devices] when the feature is disabled.
  Config? __config;
  Config get _config {
    __config ??= _configLoader();
    return __config!;
  }

  String get _defaultSchema {
    final Uri uri = _fileSystem
        .directory(Cache.flutterRoot)
        .childDirectory('packages')
        .childDirectory('flutter_tools')
        .childDirectory('lib')
        .childDirectory('src')
        .childDirectory('aurora')
        .childDirectory('static')
        .childFile('aurora-devices.schema.json')
        .uri;

    // otherwise it won't contain the Uri schema, so the file:// at the start
    // will be missing
    assert(uri.isAbsolute);

    return uri.toString();
  }

  /// Ensure the config file exists on disk by creating one with default values
  /// if it doesn't exist yet.
  ///
  /// The config file should always be present so we can give the user a path
  /// to a file they can edit.
  void ensureFileExists() {
    if (!_fileSystem.file(_config.configPath).existsSync()) {
      _config.setValue(_kSchema, _defaultSchema);
      _config.setValue(_kAuroraDevices, <dynamic>[
        AuroraDeviceConfig.getExampleForPlatform(_platform).toJson(),
      ]);
    }
  }

  List<dynamic>? _getDevicesJsonValue() {
    final dynamic json = _config.getValue(_kAuroraDevicesConfigKey);

    if (json == null) {
      return null;
    } else if (json is! List) {
      const String msg =
          "Could not load Aurora OS devices config. config['$_kAuroraDevicesConfigKey'] is not a JSON array.";
      _logger.printError(msg);
      throw const AuroraDeviceRevivalException(msg);
    }

    return json;
  }

  /// Get the list of [AuroraDeviceConfig]s that are listed in the config file
  /// including disabled ones.
  ///
  /// Throws an Exception when the config could not be loaded and logs any
  /// errors.
  List<AuroraDeviceConfig> get devices {
    final List<dynamic>? typedListNullable = _getDevicesJsonValue();
    if (typedListNullable == null) {
      return <AuroraDeviceConfig>[];
    }

    final List<dynamic> typedList = typedListNullable;
    final List<AuroraDeviceConfig> revived = <AuroraDeviceConfig>[];
    for (final MapEntry<int, dynamic> entry in typedList.asMap().entries) {
      try {
        revived.add(AuroraDeviceConfig.fromJson(entry.value));
      } on AuroraDeviceRevivalException catch (e) {
        final String msg = 'Could not load Aurora OS device from config index ${entry.key}: $e';
        _logger.printError(msg);
        throw AuroraDeviceRevivalException(msg);
      }
    }

    return revived;
  }

  /// Get the list of [AuroraDeviceConfig]s that are listed in the config file
  /// including disabled ones.
  ///
  /// Returns an empty list when the config could not be loaded and logs any
  /// errors.
  List<AuroraDeviceConfig> tryGetDevices() {
    try {
      return devices;
    } on Exception {
      // any Exceptions are logged by [devices] already.
      return <AuroraDeviceConfig>[];
    }
  }

  /// Set the list of [AuroraDeviceConfig]s in the config file.
  ///
  /// It should generally be avoided to call this often, since this could mean
  /// data loss. If you want to add or remove a device from the config,
  /// consider using [add] or [remove].
  set devices(List<AuroraDeviceConfig> configs) {
    _config.setValue(_kAuroraDevicesConfigKey, configs.map<dynamic>((AuroraDeviceConfig c) => c.toJson()).toList());
  }

  /// Add a aurora device to the config file.
  ///
  /// Works even when some of the aurora devices in the config file are not
  /// valid.
  ///
  /// May throw a [AuroraDeviceRevivalException] if `config['aurora-devices']`
  /// is not a list.
  void add(AuroraDeviceConfig config) {
    _config.setValue(_kAuroraDevicesConfigKey, <dynamic>[
      ...?_getDevicesJsonValue(),
      config.toJson(),
    ]);
  }

  /// Returns true if the config file contains a device with id [deviceId].
  bool contains(String deviceId) {
    return devices.any((AuroraDeviceConfig device) => device.id == deviceId);
  }

  /// Removes the first device with this device id from the config file.
  ///
  /// Returns true if the device was successfully removed, false if a device
  /// with this id could not be found.
  bool remove(String deviceId) {
    final List<AuroraDeviceConfig> modifiedDevices = devices;

    // we use this instead of filtering so we can detect if we actually removed
    // anything.
    final AuroraDeviceConfig? device = modifiedDevices
        .cast<AuroraDeviceConfig?>()
        .firstWhere((AuroraDeviceConfig? d) => d!.id == deviceId, orElse: () => null);

    if (device == null) {
      return false;
    }

    modifiedDevices.remove(device);
    devices = modifiedDevices;
    return true;
  }

  String get configPath => _config.configPath;
}
