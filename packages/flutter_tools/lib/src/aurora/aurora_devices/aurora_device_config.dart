// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'package:meta/meta.dart';

import '../../base/platform.dart';
import '../../build_info.dart';

/// Something went wrong while trying to load the aurora devices config from the
/// JSON representation. Maybe some value is missing, maybe something has the
/// wrong type, etc.
@immutable
class AuroraDeviceRevivalException implements Exception {
  const AuroraDeviceRevivalException(this.message);

  const AuroraDeviceRevivalException.fromDescriptions(String fieldDescription, String expectedValueDescription)
      : message = 'Expected $fieldDescription to be $expectedValueDescription.';

  final String message;

  @override
  String toString() {
    return message;
  }

  @override
  bool operator ==(Object other) {
    return (other is AuroraDeviceRevivalException) && (other.message == message);
  }

  @override
  int get hashCode => message.hashCode;
}

/// A single configured aurora device.
///
/// In the aurora devices config file on disk, there may be multiple aurora
/// devices configured.
@immutable
class AuroraDeviceConfig {
  const AuroraDeviceConfig({
    required this.host,
    required this.port,
    required this.auth,
    required this.rootPassword,
    required this.platform,
    required this.version,
    required this.enabled,
    this.emulatorId,
  }) : assert(platform == TargetPlatform.aurora_arm ||
            platform == TargetPlatform.aurora_arm64 ||
            platform == TargetPlatform.aurora_x64);

  /// Create a AuroraDeviceConfig from some JSON value.
  /// If anything fails internally (some value doesn't have the right type,
  /// some value is missing, etc) a [AuroraDeviceRevivalException] with the description
  /// of the error is thrown. (No exceptions/errors other than JsonRevivalException
  /// should ever be thrown by this factory.)
  factory AuroraDeviceConfig.fromJson(dynamic json) {
    final Map<String, dynamic> typedMap = _castJsonObject(json, 'device configuration', 'a JSON object');

    final String? archString =
        _castStringOrNull(typedMap[_kPlatform], _kPlatform, 'null or one of aurora_arm, aurora_arm64, aurora_x64');

    late TargetPlatform? platform;
    try {
      platform = archString == null ? null : getTargetPlatformForName(archString);
    } on UnsupportedError {
      throw const AuroraDeviceRevivalException.fromDescriptions(
          _kPlatform, 'null or one of aurora_arm, aurora_arm64, aurora_x64');
    }

    if (platform != null &&
        platform != TargetPlatform.aurora_arm &&
        platform != TargetPlatform.aurora_arm64 &&
        platform != TargetPlatform.aurora_x64) {
      throw const AuroraDeviceRevivalException.fromDescriptions(
          _kPlatform, 'null or one of aurora_arm, aurora_arm64, aurora_x64');
    }

    return AuroraDeviceConfig(
      host: _castString(typedMap[_kHost], _kHost, 'a string'),
      port: _castInt(typedMap[_kPort], _kPort, 'a int'),
      auth: _castString(typedMap[_kAuth], _kAuth, 'a string'),
      rootPassword: _castString(typedMap[_kRootPassword], _kRootPassword, 'a string'),
      platform: platform ?? TargetPlatform.aurora_arm,
      version: _castString(typedMap[_kVersion], _kVersion, 'a string'),
      enabled: _castBool(typedMap[_kEnabled], _kEnabled, 'a boolean'),
    );
  }

  static const String _kHost = 'host';
  static const String _kPort = 'port';
  static const String _kAuth = 'auth';
  static const String _kRootPassword = 'rootPassword';
  static const String _kPlatform = 'platform';
  static const String _kVersion = 'version';
  static const String _kEnabled = 'enabled';

  /// An example device config used for creating the default config file.
  static const AuroraDeviceConfig exampleUnix = AuroraDeviceConfig(
    host: '192.168.2.15',
    port: 22,
    auth: '/home/user/.ssh/id_rsa',
    rootPassword: '00000',
    platform: TargetPlatform.aurora_arm,
    version: '4.0.2.303',
    enabled: true,
  );

  /// Returns an example aurora device config that works on the given host platform.
  static AuroraDeviceConfig getExampleForPlatform(Platform platform) {
    if (platform.isLinux) {
      return exampleUnix;
    }
    throw UnsupportedError('Unsupported operating system');
  }

  final String host;
  final int port;
  final String auth;
  final String? rootPassword;
  final TargetPlatform platform;
  final String version;
  final bool enabled;
  final String? emulatorId;

  String get id => isEmulator ? emulatorId! : 'aurora-$host';

  String get label => isEmulator ? 'Emulator Aurora OS ($version)' : 'Device Aurora OS';

  bool get isEmulator => emulatorId != null;

  /// Invokes and returns the result of [closure].
  ///
  /// If anything at all is thrown when executing the closure, a
  /// [AuroraDeviceRevivalException] is thrown with the given [fieldDescription] and
  /// [expectedValueDescription].
  static T _maybeRethrowAsRevivalException<T>(
      T Function() closure, String fieldDescription, String expectedValueDescription) {
    try {
      return closure();
    } on Object {
      throw AuroraDeviceRevivalException.fromDescriptions(fieldDescription, expectedValueDescription);
    }
  }

  /// Tries to make a string-keyed, non-null map from [value].
  ///
  /// If the value is null or not a valid string-keyed map, a [AuroraDeviceRevivalException]
  /// with the given [fieldDescription] and [expectedValueDescription] is thrown.
  static Map<String, dynamic> _castJsonObject(dynamic value, String fieldDescription, String expectedValueDescription) {
    if (value == null) {
      throw AuroraDeviceRevivalException.fromDescriptions(fieldDescription, expectedValueDescription);
    }

    return _maybeRethrowAsRevivalException(
      () => Map<String, dynamic>.from(value as Map<dynamic, dynamic>),
      fieldDescription,
      expectedValueDescription,
    );
  }

  /// Tries to cast [value] to a bool.
  ///
  /// If the value is null or not a bool, a [CustomDeviceRevivalException] with the given
  /// [fieldDescription] and [expectedValueDescription] is thrown.
  static bool _castBool(dynamic value, String fieldDescription, String expectedValueDescription) {
    if (value == null) {
      throw AuroraDeviceRevivalException.fromDescriptions(fieldDescription, expectedValueDescription);
    }
    return _maybeRethrowAsRevivalException(() => value as bool, fieldDescription, expectedValueDescription);
  }

  /// Tries to cast [value] to a int.
  ///
  /// If the value is null or not a String, a [AuroraDeviceRevivalException] with the given
  /// [fieldDescription] and [expectedValueDescription] is thrown.
  static int _castInt(dynamic value, String fieldDescription, String expectedValueDescription) {
    if (value == null) {
      throw AuroraDeviceRevivalException.fromDescriptions(fieldDescription, expectedValueDescription);
    }

    return _maybeRethrowAsRevivalException(
      () => int.parse(value.toString()),
      fieldDescription,
      expectedValueDescription,
    );
  }

  /// Tries to cast [value] to a String.
  ///
  /// If the value is null or not a String, a [AuroraDeviceRevivalException] with the given
  /// [fieldDescription] and [expectedValueDescription] is thrown.
  static String _castString(dynamic value, String fieldDescription, String expectedValueDescription) {
    if (value == null) {
      throw AuroraDeviceRevivalException.fromDescriptions(fieldDescription, expectedValueDescription);
    }

    return _maybeRethrowAsRevivalException(
      () => value as String,
      fieldDescription,
      expectedValueDescription,
    );
  }

  /// Tries to cast [value] to a nullable String.
  ///
  /// If the value not null and not a String, a [AuroraDeviceRevivalException] with the given
  /// [fieldDescription] and [expectedValueDescription] is thrown.
  static String? _castStringOrNull(dynamic value, String fieldDescription, String expectedValueDescription) {
    if (value == null) {
      return null;
    }

    return _castString(value, fieldDescription, expectedValueDescription);
  }

  Object toJson() {
    return <String, Object?>{
      _kHost: host,
      _kPort: port,
      _kAuth: auth,
      _kRootPassword: rootPassword,
      _kPlatform: getNameForTargetPlatform(platform),
      _kVersion: version,
      _kEnabled: enabled,
    };
  }

  AuroraDeviceConfig copyWith({
    String? host,
    int? port,
    String? auth,
    String? rootPassword,
    TargetPlatform? platform,
    String? version,
    bool? enabled,
  }) {
    return AuroraDeviceConfig(
      host: host ?? this.host,
      port: port ?? this.port,
      auth: auth ?? this.auth,
      rootPassword: rootPassword ?? this.rootPassword,
      platform: platform ?? this.platform,
      version: version ?? this.version,
      enabled: enabled ?? this.enabled,
    );
  }

  @override
  bool operator ==(Object other) {
    return other is AuroraDeviceConfig &&
        other.host == host &&
        other.port == port &&
        other.auth == auth &&
        other.rootPassword == rootPassword &&
        other.platform == platform &&
        other.version == version &&
        other.enabled == enabled;
  }

  @override
  int get hashCode {
    return host.hashCode ^
        port.hashCode ^
        auth.hashCode ^
        rootPassword.hashCode ^
        platform.hashCode ^
        version.hashCode ^
        enabled.hashCode;
  }

  @override
  String toString() {
    return 'AuroraDeviceConfig('
        'host: $host, '
        'port: $port, '
        'auth: $auth, '
        'rootPassword: $rootPassword, '
        'platform: $platform, '
        'version: $version, '
        'enabled: $enabled)';
  }
}
