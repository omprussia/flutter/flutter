// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'dart:async';
import 'dart:io';

import 'package:async/async.dart';
import 'package:meta/meta.dart';
import 'package:process/process.dart';

import '../../application_package.dart';
import '../../base/common.dart';
import '../../base/file_system.dart';
import '../../base/logger.dart';
import '../../base/process.dart';
import '../../base/utils.dart';
import '../../build_info.dart';
import '../../bundle_builder.dart';
import '../../convert.dart';
import '../../device.dart';
import '../../device_port_forwarder.dart';
import '../../features.dart';
import '../../globals.dart' as globals;
import '../../project.dart';
import '../../protocol_discovery.dart';
import '../application_package.dart';
import '../aurora_emulators.dart';
import '../build_aurora.dart';
import '../platform/aurora_psdk.dart';
import 'aurora_device_config.dart';
import 'aurora_device_workflow.dart';
import 'aurora_devices_config.dart';

/// Common timeout ssh connect
const int timeoutSSH = 3;

/// Replace all occurrences of `${someName}` with the value found for that
/// name inside replacementValues or additionalReplacementValues.
///
/// The replacement value is first looked for in [replacementValues] and then
/// [additionalReplacementValues]. If no value is found, an empty string will be
/// substituted instead.
List<String> interpolateCommand(List<String> command, Map<String, String> replacementValues,
    {Map<String, String> additionalReplacementValues = const <String, String>{}}) {
  return interpolateStringList(command, Map<String, String>.of(additionalReplacementValues)..addAll(replacementValues));
}

/// Preparation of request for device
List<String> getExecuteSSH(
  String command, {
  required AuroraDeviceConfig config,
  String user = 'defaultuser',
}) {
  if (user == 'root') {
    if (!config.isEmulator) {
      user = 'defaultuser';
      command = 'echo ${config.rootPassword} | devel-su $command';
    }
  }
  return <String>[
    'ssh',
    '-o',
    'ConnectTimeout=$timeoutSSH',
    '-o',
    'StrictHostKeyChecking=no',
    '-i',
    config.auth,
    '-p${config.port}',
    '$user@${config.host}',
    command
  ];
}

/// A log reader that can listen to a process' stdout / stderr or another log line Stream.
class AuroraDeviceLogReader extends DeviceLogReader {
  AuroraDeviceLogReader(this.name);

  /// The name of the device this log reader is associated with.
  @override
  final String name;

  /// PID process run application
  int? _pidFd;

  /// Listen file for CLI command logs
  File get fd => globals.fs.file(pathFdByPid(_pidFd));

  @visibleForTesting
  final StreamController<String> logLinesController = StreamController<String>.broadcast();

  @visibleForTesting
  final List<StreamSubscription<String>> subscriptions = <StreamSubscription<String>>[];

  /// Create path to file FD with logs
  String pathFdByPid(dynamic pid) {
    return '/tmp/$pid-fd';
  }

  /// Create fd after success run application
  Future<void> createFd(int pid) {
    _pidFd = pid;
    return fd.create(recursive: true);
  }

  /// Listen to [process]' stdout and stderr, decode them using [SystemEncoding]
  /// and add each decoded line to [logLines].
  ///
  /// However, [logLines] will not be done when the [process]' stdout and stderr
  /// streams are done. So [logLines] will still be alive after the process has
  /// finished.
  ///
  /// See [AuroraDeviceLogReader.dispose] to end the [logLines] stream.
  void listenToProcessOutput(Process process, {Encoding encoding = systemEncoding}) {
    final Converter<List<int>, String> decoder = encoding.decoder;

    subscriptions.add(process.stdout.transform<String>(decoder).transform<String>(const LineSplitter()).listen(
      (String line) async {
        await sendMessageToFd(process.pid, line);
        logLinesController.add(line);
      },
    ));

    subscriptions.add(
      process.stderr.transform<String>(decoder).transform<String>(const LineSplitter()).listen(
        (String line) async {
          await sendMessageToFd(process.pid, line);
          logLinesController.add(line);
        },
      ),
    );
  }

  /// Append message for CLI logs
  Future<void> sendMessageToFd(int pid, String message) async {
    if (await fd.exists()) {
      await fd.writeAsString('$message\n', mode: FileMode.append);
    }
  }

  /// Add all lines emitted by [lines] to this [AuroraDeviceLogReader]s [logLines] stream.
  ///
  /// Similar to [listenToProcessOutput], [logLines] will not be marked as done when the argument stream is done.
  ///
  /// Useful when you want to combine the contents of multiple log readers.
  void listenToLinesStream(Stream<String> lines) {
    subscriptions.add(lines.listen(logLinesController.add));
  }

  /// Dispose this log reader, freeing all associated resources and marking [logLines] as done.
  @override
  Future<void> dispose() async {
    final List<Future<void>> futures = <Future<void>>[];

    for (final StreamSubscription<String> subscription in subscriptions) {
      futures.add(subscription.cancel());
    }

    futures.add(logLinesController.close());

    await Future.wait(futures);
  }

  @override
  Stream<String> get logLines => logLinesController.stream;
}

/// A [DevicePortForwarder] that uses commands to forward / unforward a port.
class AuroraDevicePortForwarder extends DevicePortForwarder {
  AuroraDevicePortForwarder({
    required AuroraDeviceConfig config,
    required ProcessManager processManager,
    required Logger logger,
  })  : _config = config,
        _processManager = processManager,
        _processUtils = ProcessUtils(processManager: processManager, logger: logger);

  final AuroraDeviceConfig _config;
  final ProcessManager _processManager;
  final ProcessUtils _processUtils;
  final List<ForwardedPort> _forwardedPorts = <ForwardedPort>[];

  final int numTries = 3;

  @override
  Future<void> dispose() async {
    // Copy the list so we don't modify it concurrently
    await Future.wait(List<ForwardedPort>.of(_forwardedPorts).map(unforward));
  }

  @override
  Future<int> forward(int devicePort, {int? hostPort}) async {
    int actualHostPort = (hostPort == 0 || hostPort == null) ? devicePort : hostPort;
    int tries = 0;

    while (tries < numTries) {
      // When the desired host port is already forwarded by this Forwarder, choose another one
      while (_forwardedPorts.any((ForwardedPort port) => port.hostPort == actualHostPort)) {
        actualHostPort += 1;
      }

      final ForwardedPort? port = await tryForward(devicePort, actualHostPort);

      if (port != null) {
        _forwardedPorts.add(port);
        return actualHostPort;
      } else {
        // null value means the forwarding failed (for whatever reason) increase port by one and try again
        actualHostPort += 1;
        tries += 1;
      }
    }

    throw ToolExit('Forwarding port for custom device ${_config.label} failed after $tries tries.');
  }

  @override
  List<ForwardedPort> get forwardedPorts => List<ForwardedPort>.unmodifiable(_forwardedPorts);

  @override
  Future<void> unforward(ForwardedPort forwardedPort) async {
    assert(_forwardedPorts.contains(forwardedPort));

    // Since a forwarded port represents a running process launched with
    // the forwardPortCommand, unforwarding is as easy as killing the process
    final int? pid = forwardedPort.context?.pid;
    if (pid != null) {
      _processManager.killPid(pid);
    }
    _forwardedPorts.remove(forwardedPort);
  }

  Future<ForwardedPort?> tryForward(int devicePort, int hostPort) async {
    // Clear old connect if exist
    await _processUtils.run(<String>[
      'ssh-keygen',
      '-f',
      '${Platform.environment['HOME']}/.ssh/known_hosts',
      '-R',
      '[${_config.host}]:${_config.port}',
    ]);
    // Launch the forwarding command
    final Process process = await _processUtils.start(<String>[
      'ssh',
      '-o',
      'ConnectTimeout=$timeoutSSH',
      '-o',
      'StrictHostKeyChecking=no',
      '-i',
      _config.auth,
      '-L',
      '$hostPort:127.0.0.1:$devicePort',
      'defaultuser@${_config.host}',
      '-p${_config.port}'
    ]);

    final Completer<ForwardedPort?> completer = Completer<ForwardedPort?>();

    // Read the outputs of the process; if we find a line that matches
    // the configs forwardPortSuccessRegex, we complete with a successfully
    // forwarded port.
    // If that regex never matches, this will potentially run forever
    // and the forwarding will never complete.
    final AuroraDeviceLogReader reader = AuroraDeviceLogReader(_config.label)..listenToProcessOutput(process);
    final StreamSubscription<String> logLinesSubscription = reader.logLines.listen((String line) {
      if (!completer.isCompleted) {
        completer.complete(ForwardedPort.withContext(hostPort, devicePort, process));
      }
    });

    // if the process exits (even with exitCode == 0), that is considered
    // a port forwarding failure and we complete with a null value.
    unawaited(process.exitCode.whenComplete(() {
      if (!completer.isCompleted) {
        completer.complete();
      }
    }));

    unawaited(completer.future.whenComplete(() {
      unawaited(logLinesSubscription.cancel());
      unawaited(reader.dispose());
    }));

    return completer.future;
  }
}

/// A combination of [ApplicationPackage] and a [AuroraDevice]. Can only start,
/// stop this specific app package with this specific device. Useful because we
/// often need to store additional context to an app that is running on a device,
/// like any forwarded ports we need to unforward later, the process we need to
/// kill to stop the app, maybe other things in the future.
class AuroraDeviceAppSession {
  AuroraDeviceAppSession(
    this.appName,
    this.packageName,
    this.projectPath,
    this.psdk, {
    required AuroraDevice device,
    required Logger logger,
    required ProcessManager processManager,
  })  : _device = device,
        _logger = logger,
        _processManager = processManager,
        _processUtils = ProcessUtils(processManager: processManager, logger: logger),
        logReader = AuroraDeviceLogReader(appName);

  final String appName;
  final String packageName;
  final String projectPath;
  final AuroraPSDK psdk;
  final AuroraDevice _device;
  final Logger _logger;
  final ProcessManager _processManager;
  final ProcessUtils _processUtils;
  final AuroraDeviceLogReader logReader;

  Process? _process;
  int? _forwardedHostPort;

  /// Get the engine options for the given [debuggingOptions], [traceStartup] and [route].
  ///
  /// [debuggingOptions] and [route] can be null.
  ///
  /// For example, `_getEngineOptions(null, false, null)` will return `['enable-dart-profiling=true']`
  List<String> _getEngineOptions(DebuggingOptions debuggingOptions, bool traceStartup, String? route) {
    final String dartVmFlags = computeDartVmFlags(debuggingOptions);
    return <String>[
      if (traceStartup) 'trace-startup=true',
      if (route != null) 'route=$route',
      if (debuggingOptions.enableDartProfiling) 'enable-dart-profiling=true',
      if (debuggingOptions.enableSoftwareRendering) 'enable-software-rendering=true',
      if (debuggingOptions.skiaDeterministicRendering) 'skia-deterministic-rendering=true',
      if (debuggingOptions.traceSkia) 'trace-skia=true',
      if (debuggingOptions.traceAllowlist != null) 'trace-allowlist=${debuggingOptions.traceAllowlist}',
      if (debuggingOptions.traceSystrace) 'trace-systrace=true',
      if (debuggingOptions.endlessTraceBuffer) 'endless-trace-buffer=true',
      if (debuggingOptions.dumpSkpOnShaderCompilation) 'dump-skp-on-shader-compilation=true',
      if (debuggingOptions.cacheSkSL) 'cache-sksl=true',
      if (debuggingOptions.purgePersistentCache) 'purge-persistent-cache=true',
      if (debuggingOptions.debuggingEnabled) ...<String>[
        if (debuggingOptions.deviceVmServicePort != null) 'vm-service-port=${debuggingOptions.deviceVmServicePort}',
        if (debuggingOptions.buildInfo.isDebug) ...<String>[
          'enable-checked-mode=true',
          'verify-entry-points=true',
        ],
        if (debuggingOptions.startPaused) 'start-paused=true',
        if (debuggingOptions.disableServiceAuthCodes) 'disable-service-auth-codes=true',
        if (dartVmFlags.isNotEmpty) 'dart-flags=$dartVmFlags',
        if (debuggingOptions.useTestFonts) 'use-test-fonts=true',
        if (debuggingOptions.verboseSystemLogs) 'verbose-logging=true',
      ],
    ];
  }

  /// Get the engine options for the given [debuggingOptions], [traceStartup] and [route].
  ///
  /// [debuggingOptions] and [route] can be null.
  ///
  /// For example, `_getEngineOptionsForCmdline(null, false, null)` will return `--enable-dart-profiling=true`
  String _getEngineOptionsForCmdline(DebuggingOptions debuggingOptions, bool traceStartup, String? route) {
    return _getEngineOptions(debuggingOptions, traceStartup, route).map((String e) => '--$e').join(' ');
  }

  /// Start the app on the device.
  /// Needs the app to be installed on the device and not running already.
  ///
  /// [route], [debuggingOptions], [platformArgs] and
  /// [userIdentifier] may be null.
  Future<LaunchResult> start({
    String? route,
    required DebuggingOptions debuggingOptions,
    Map<String, Object?> platformArgs = const <String, Object>{},
  }) async {
    final bool traceStartup = platformArgs['trace-startup'] as bool? ?? false;

    final List<String> runDebug = getExecuteSSH(
      'env LD_LIBRARY_PATH=/opt/app/$packageName/current/data/lib /usr/bin/$packageName',
      config: _device._config,
    );
    final List<String> runRelease = getExecuteSSH(
      'invoker --type=qt5 $packageName',
      config: _device._config,
    );

    final List<String> interpolated = interpolateCommand(
      debuggingOptions.buildInfo.mode == BuildMode.release ? runRelease : runDebug,
      <String, String>{
        'engineOptions': _getEngineOptionsForCmdline(debuggingOptions, traceStartup, route),
      },
    );

    final Process process = await _processUtils.start(interpolated);
    assert(_process == null);
    _process = process;

    final ProtocolDiscovery discovery = ProtocolDiscovery.vmService(
      logReader,
      portForwarder: _device.portForwarder,
      logger: _logger,
      ipv6: false,
    );

    // We need to make the discovery listen to the logReader before the logReader
    // listens to the process output since logReader.lines is a broadcast stream
    // and events may be discarded.
    // Whether that actually happens is another thing since this is all executed
    // in the same microtask AFAICT but this way we're on the safe side.
    logReader.listenToProcessOutput(process);
    // Create fd file for listen logs
    await logReader.createFd(process.pid);

    final Uri? vmServiceUri = await discovery.uri;
    await discovery.cancel();
    _forwardedHostPort = vmServiceUri?.port;
    return LaunchResult.succeeded(vmServiceUri: vmServiceUri);
  }

  void _tryUnforwardPort() {
    if (_forwardedHostPort != null) {
      final ForwardedPort forwardedPort =
          _device.portForwarder.forwardedPorts.singleWhere((ForwardedPort forwardedPort) {
        return forwardedPort.hostPort == _forwardedHostPort;
      });
      _forwardedHostPort = null;
      _device.portForwarder.unforward(forwardedPort);
    }
  }

  /// Stop application by package from SSH connect
  Future<void> _stopRemote(String package) async {
    final RunResult result = await _processUtils.run(getExecuteSSH(
      'ps ax | grep $package',
      config: _device._config,
    ));
    result.stdout
        .split('\n')
        .where((String e) => !e.contains('ps ax') && e.contains('/usr/bin/$package'))
        .forEach((String line) async {
      final String? pid = line.trim().split(' ').firstOrNull;
      if (pid != null) {
        await _processUtils.run(getExecuteSSH(
          'kill -9 $pid',
          config: _device._config,
        ));
      }
    });
  }

  /// Remove temp file logs
  Future<void> _removePidFd(int pid) async {
    final File file = logReader.fd;
    if (await file.exists()) {
      await file.delete();
    }
  }

  /// Stop the app on the device.
  /// Returns false if the app is not yet running. Also unforwards any
  /// forwarded ports.
  Future<bool> stop() async {
    if (_process == null) {
      return false;
    }
    _tryUnforwardPort();
    final bool result = _processManager.killPid(_process!.pid);
    await _stopRemote(packageName);
    await _removePidFd(_process!.pid);
    _process = null;
    return result;
  }

  Future<void> dispose() async {
    if (_process != null) {
      _tryUnforwardPort();
      _processManager.killPid(_process!.pid);
      _process = null;
    }

    unawaited(logReader.dispose());
  }
}

/// A device that uses user-configured actions for the common device methods.
/// The exact actions are defined by the contents of the [AuroraDeviceConfig]
/// used to construct it.
class AuroraDevice extends Device {
  AuroraDevice({
    required AuroraDeviceConfig config,
    required Logger logger,
    required ProcessManager processManager,
  })  : _config = config,
        _logger = logger,
        _processManager = processManager,
        _processUtils = ProcessUtils(processManager: processManager, logger: logger),
        _globalLogReader = AuroraDeviceLogReader(config.label),
        portForwarder = AuroraDevicePortForwarder(
          config: config,
          processManager: processManager,
          logger: logger,
        ),
        super(config.id, logger: logger, category: Category.mobile, ephemeral: true, platformType: PlatformType.aurora);

  final AuroraDeviceConfig _config;
  final Logger _logger;
  final ProcessManager _processManager;
  final ProcessUtils _processUtils;
  final Map<String, AuroraDeviceAppSession> _sessions = <String, AuroraDeviceAppSession>{};
  final AuroraDeviceLogReader _globalLogReader;

  @override
  final DevicePortForwarder portForwarder;

  String _getProjectPath() {
    return FlutterProject.current().directory.path;
  }

  Future<String?> _getPackageName(String projectPath) async {
    final Directory specDir = globals.fs.directory(globals.fs.path.join(projectPath, 'aurora', 'rpm'));
    if (!await specDir.exists()) {
      return null;
    }
    final FileSystemEntity? specFile =
        await specDir.list().where((FileSystemEntity element) => element.basename.endsWith('.spec')).firstOrNull;
    if (specFile == null) {
      return null;
    }
    final File file = globals.fs.file(specFile.path);
    if (!await file.exists()) {
      return null;
    }
    final String packageName = await file
        .openRead()
        .transform(utf8.decoder)
        .transform(const LineSplitter())
        .firstWhere((String line) => line.contains('Name: '), orElse: () => '');
    if (packageName.isEmpty) {
      return null;
    } else {
      return packageName.replaceAll('Name: ', '');
    }
  }

  Future<AuroraDeviceAppSession?> _getAppSession(String? appName) async {
    final AuroraDeviceAppSession? session = _sessions[appName];
    if (session == null && !await _createAppSession(appName)) {
      return null;
    }
    return _sessions[appName];
  }

  Future<bool> _createAppSession(
    String? appName,
  ) async {
    if (appName == null) {
      return false;
    }
    final String projectPath = _getProjectPath();
    final String? packageName = await _getPackageName(projectPath);
    if (packageName == null) {
      return false;
    }
    try {
      final AuroraPSDK psdk = await AuroraPSDK.getInstance();
      _sessions.putIfAbsent(appName, () {
        final AuroraDeviceAppSession session = AuroraDeviceAppSession(
          appName,
          packageName,
          projectPath,
          psdk,
          device: this,
          logger: _logger,
          processManager: _processManager,
        );
        _globalLogReader.listenToLinesStream(session.logReader.logLines);
        return session;
      });
    } on Exception catch (e) {
      _logger.printError(e.toString());
      return false;
    }
    return true;
  }

  Future<bool> tryPing({Duration? timeout, Map<String, String> replacementValues = const <String, String>{}}) async {
    final RunResult result = await _processUtils.run(
      <String>[
        'nc',
        '-w',
        timeout?.inSeconds.toString() ?? '2',
        '-vz',
        _config.host,
        _config.port.toString(),
      ],
    );
    return result.exitCode == 0;
  }

  List<String> getRemotePaths(List<String> paths) {
    final List<String> remotePath = <String>[];
    for (final String path in paths) {
      remotePath.add('/home/defaultuser/Downloads/${globals.fs.file(path).basename}');
    }
    return remotePath;
  }

  Future<bool> tryUploadFiles(List<String> localPaths, List<String> remotePaths) async {
    if (localPaths.length != remotePaths.length) {
      return false;
    }
    for (final (int index, String localPath) in localPaths.indexed) {
      final RunResult result = await _processUtils.run(
        <String>[
          'scp',
          '-o',
          'ConnectTimeout=$timeoutSSH',
          '-o',
          'StrictHostKeyChecking=no',
          '-i',
          _config.auth,
          '-P${_config.port}',
          localPath,
          'defaultuser@${_config.host}:${remotePaths[index]}',
        ],
      );
      if (result.exitCode != 0) {
        return false;
      }
    }
    return true;
  }

  Future<bool> tryRemoveUploadFiles(List<String> remotePaths) async {
    for (final String remotePath in remotePaths) {
      final RunResult result = await _processUtils.run(getExecuteSSH(
        'rm "$remotePath"',
        config: _config,
        user: 'root',
      ));
      if (result.exitCode != 0) {
        return false;
      }
    }
    return true;
  }

  Future<bool> isInstall(String package) async {
    final RunResult result = await _processUtils.run(getExecuteSSH(
      'ls /usr/bin/$package',
      config: _config,
    ));
    return result.exitCode == 0;
  }

  Future<bool> tryInstallApm(String remotePath) async {
    RunResult result = await _processUtils.run(getExecuteSSH(
      'gdbus call --system --dest ru.omp.APM --object-path /ru/omp/APM --method ru.omp.APM.Install "$remotePath" {}',
      config: _config,
    ));
    // @todo Old variant install without {}
    if (result.exitCode != 0) {
      result = await _processUtils.run(getExecuteSSH(
        'gdbus call --system --dest ru.omp.APM --object-path /ru/omp/APM --method ru.omp.APM.Install "$remotePath"',
        config: _config,
      ));
      if (result.exitCode != 0) {
        _logger.printError('Error install package.');
      }
    }
    return result.exitCode == 0;
  }

  Future<bool> tryUninstallApm(String package) async {
    RunResult result = await _processUtils.run(getExecuteSSH(
      'gdbus call --system --dest ru.omp.APM --object-path /ru/omp/APM --method ru.omp.APM.Remove "$package" {}',
      config: _config,
    ));
    // @todo Old variant delete without {}
    if (result.exitCode != 0) {
      result = await _processUtils.run(getExecuteSSH(
        'gdbus call --system --dest ru.omp.APM --object-path /ru/omp/APM --method ru.omp.APM.Remove "$package"',
        config: _config,
      ));
      if (result.exitCode != 0) {
        _logger.printError('Error uninstall package.');
      }
    }
    return result.exitCode == 0;
  }

  Future<bool> tryInstallPkcon(List<String> remotePaths) async {
    for (final String remotePath in remotePaths) {
      final RunResult result = await _processUtils.run(getExecuteSSH(
        'pkcon -y install-local $remotePath',
        config: _config,
        user: 'root',
      ));
      if (result.exitCode != 0) {
        return false;
      }
    }
    return true;
  }

  Future<bool> tryUninstallPkcon(String package) async {
    final RunResult result = await _processUtils.run(getExecuteSSH(
      'pkcon -y remove $package',
      config: _config,
      user: 'root',
    ));
    return result.exitCode == 0;
  }

  Future<bool> tryInstall(
    String appName,
    String packageName,
    BuildInfo buildInfo,
    TargetPlatform devicePlatform,
  ) async {
    final AuroraDeviceAppSession? session = await _getAppSession(appName);
    if (session == null) {
      return false;
    }
    // get rpms build
    List<String> rpms = await getRPMs(
      devicePlatform,
      buildInfo,
      projectPath: session.projectPath,
    );
    if (rpms.isEmpty) {
      _logger.printError('No rpm files found for installation: ${getAuroraBuildDirectory(devicePlatform, buildInfo)}');
      return false;
    }

    // For APM install one rpm file
    if (_config.version.startsWith('5.')) {
      rpms = <String>[rpms.first];
    }

    // Sign packages
    if (!await session.psdk.trySignPackages(rpms)) {
      _logger.printError('Failed to sign the RPM packages.');
      _logger.printWarning(
          'The keys must be specified in the flutter configuration file. More details: `flutter-aurora config --help`.');
      return false;
    }

    // Get pates rpm packages on device
    final List<String> remotePaths = getRemotePaths(rpms);
    // Clear before upload, files could have been uploaded by a root user
    await tryRemoveUploadFiles(remotePaths);
    // Upload files to device
    if (!await tryUploadFiles(rpms, remotePaths)) {
      _logger.printError('Error upload files.');
      return false;
    }

    // Remove if install
    if (!await tryUninstall(packageName)) {
      _logger.printError('Error uninstall package.');
      return false;
    }

    // Install for 5 - APM and for 4 - pkcon
    bool installResult = false;
    if (_config.version.startsWith('5.')) {
      if (await tryInstallApm(remotePaths.first)) {
        installResult = true;
      }
    } else {
      if (await tryInstallPkcon(remotePaths)) {
        installResult = true;
      }
    }

    // Remove upload files
    await tryRemoveUploadFiles(remotePaths);

    // Success install
    return installResult;
  }

  Future<bool> tryUninstall(String packageName) async {
    if (!await isInstall(packageName)) {
      return true;
    }
    if (await tryUninstallPkcon(packageName)) {
      return true;
    } else if (await tryUninstallApm(packageName)) {
      return true;
    }
    return false;
  }

  @override
  void clearLogs() {}

  @override
  Future<void> dispose() async {
    _sessions
      ..forEach((_, AuroraDeviceAppSession session) => session.dispose())
      ..clear();
  }

  @override
  Future<String?> get emulatorId async => _config.isEmulator ? _config.id : null;

  @override
  FutureOr<DeviceLogReader> getLogReader({ApplicationPackage? app, bool includePastLogs = false}) async {
    if (app != null) {
      if (includePastLogs) {
        final AuroraDeviceAppSession? session = await _getAppSession(app.name);
        if (session != null) {
          final RunResult result = await _processUtils.run(
            <String>[
              'ps',
              'aux',
            ],
          );
          String? pid;
          final RegExp isNum = RegExp(r'^[0-9]+$');
          for (final String line in result.stdout.trim().split('\n')) {
            if (line.contains('/usr/bin/${session.packageName}')) {
              if (pid == null) {
                pid = line.split(' ').where((String element) => isNum.hasMatch(element)).firstOrNull;
              } else {
                throwToolExit('Keep only one copy of the application for listening to logs.');
              }
            }
          }
          final String pathFd = session.logReader.pathFdByPid(pid);
          if (pid != null && await globals.fs.file(pathFd).exists()) {
            final Process process = await _processUtils.start(<String>[
              'tail',
              '-n',
              '1',
              '-s',
              '0.1',
              '-f',
              pathFd,
            ]);
            session.logReader.listenToProcessOutput(process);
          } else {
            await Future<void>.delayed(const Duration(seconds: 1));
            return getLogReader(app: app, includePastLogs: includePastLogs);
          }
          return session.logReader;
        } else {
          throwToolExit('Unable to get package name to run log, try running command in project directory.');
        }
      } else {
        return (await _getAppSession(app.name))?.logReader ?? _globalLogReader;
      }
    }
    return _globalLogReader;
  }

  @override
  Future<bool> installApp(ApplicationPackage app, {String? userIdentifier}) async {
    final String? appName = app.name;
    final AuroraDeviceAppSession? session = await _getAppSession(appName);
    final TargetPlatform devicePlatform = await targetPlatform;
    final String? packageName = session?.packageName;
    final BuildInfo? buildInfo = (app as AuroraApp).buildInfo;

    if (appName == null || packageName == null || buildInfo == null) {
      return false;
    }

    return tryInstall(
      appName,
      packageName,
      buildInfo,
      devicePlatform,
    );
  }

  @override
  Future<bool> uninstallApp(ApplicationPackage app, {String? userIdentifier}) async {
    final String? appName = app.name;
    if (appName == null) {
      return false;
    }
    final String? packageName = (await _getAppSession(appName))?.packageName;
    if (packageName == null) {
      return false;
    }
    return tryUninstall(packageName);
  }

  @override
  Future<bool> isAppInstalled(ApplicationPackage app, {String? userIdentifier}) async {
    final String? packageName = (await _getAppSession(app.name))?.packageName;
    if (packageName == null) {
      return false;
    }
    return isInstall(packageName);
  }

  @override
  Future<bool> isLatestBuildInstalled(ApplicationPackage app) async {
    return false;
  }

  @override
  Future<bool> get isLocalEmulator async => _config.isEmulator;

  @override
  bool isSupported() {
    return true;
  }

  @override
  bool get supportsScreenshot => true;

  @override
  Future<void> takeScreenshot(File outputFile) async {
    if (!await globals.fs.directory(outputFile.parent).exists()) {
      throw Exception('No such directory: ${outputFile.parent.path}');
    }
    if (_config.isEmulator) {
      return AuroraEmulator(_config.emulatorId!).takeScreenshot(outputFile);
    }
    Future<void> runProcess(List<String> cmd) async {
      final RunResult result = await _processUtils.run(cmd);
      if (result.exitCode != 0) {
        final String message = result.stderr.isNotEmpty ? result.stderr : result.stdout;
        if (message.isNotEmpty) {
          result.throwException('Taking screenshot error:\n$message');
        }
        result.throwException('Taking screenshot failed without a message');
      }
    }

    final int currentTimestamp = DateTime.now().millisecondsSinceEpoch;
    final String deviceScreenshotFilename = '/home/defaultuser/Pictures/Screenshots/screenshot-$currentTimestamp.jpg';

    await runProcess(getExecuteSSH(
      'dbus-send --print-reply --session '
      '--dest=org.nemomobile.lipstick /org/nemomobile/lipstick/screenshot '
      'org.nemomobile.lipstick.saveScreenshot string:"$deviceScreenshotFilename"',
      config: _config,
      user: 'root',
    ));

    await runProcess(<String>[
      'scp',
      '-o',
      'ConnectTimeout=$timeoutSSH',
      '-o',
      'StrictHostKeyChecking=no',
      '-i',
      _config.auth,
      '-P${_config.port}',
      'defaultuser@${_config.host}:$deviceScreenshotFilename',
      outputFile.path,
    ]);

    await runProcess(getExecuteSSH(
      'rm $deviceScreenshotFilename',
      config: _config,
    ));
  }

  @override
  bool isSupportedForProject(FlutterProject flutterProject) {
    return true;
  }

  @override
  FutureOr<bool> supportsRuntimeMode(BuildMode buildMode) {
    return <BuildMode>[BuildMode.debug, BuildMode.profile, BuildMode.release].contains(buildMode);
  }

  @override
  String get name => _config.label;

  @override
  Future<String> get sdkNameAndVersion => Future<String>.value(_config.label);

  @override
  Future<LaunchResult> startApp(
    ApplicationPackage package, {
    String? mainPath,
    String? route,
    required DebuggingOptions debuggingOptions,
    Map<String, Object?> platformArgs = const <String, Object>{},
    bool prebuiltApplication = false,
    bool ipv6 = false,
    String? userIdentifier,
    BundleBuilder? bundleBuilder,
  }) async {
    try {
      final AuroraDeviceAppSession? session = await _getAppSession(package.name);
      if (session == null) {
        return LaunchResult.failed();
      }
      // Build project
      if (platformArgs['build'] != false) {
        final TargetPlatform devicePlatform = await targetPlatform;
        final bool? isHasTarget = session.psdk.getArchPlatforms()?.contains(devicePlatform);
        if (isHasTarget == null || !isHasTarget) {
          return LaunchResult.failed();
        }
        if (!(await session.psdk.checkEmbedder(devicePlatform))) {
          return LaunchResult.failed();
        }
        final FlutterProject flutterProject = FlutterProject.current();
        await buildAurora(
          session.psdk,
          flutterProject.aurora,
          devicePlatform,
          mainPath ?? 'lib/main.dart',
          debuggingOptions.buildInfo,
        );
      }
      // Install the app on the device
      if (!await installApp(package, userIdentifier: userIdentifier)) {
        return LaunchResult.failed();
      }
      // Finally launch the app
      return await session.start(
        route: route,
        debuggingOptions: debuggingOptions,
        platformArgs: platformArgs,
      );
    } on Exception catch (e) {
      _logger.printError(e.toString());
      return LaunchResult.failed();
    }
  }

  @override
  Future<bool> stopApp(ApplicationPackage? app, {String? userIdentifier}) async {
    if (app == null) {
      return false;
    }
    final AuroraDeviceAppSession? session = await _getAppSession(app.name);
    return await session?.stop() ?? false;
  }

  @override
  Future<TargetPlatform> get targetPlatform async => _config.platform;
}

/// A [PollingDeviceDiscovery] that'll try to ping all enabled devices in the argument
/// [AuroraDevicesConfig] and report the ones that were actually reachable.
class AuroraDevices extends PollingDeviceDiscovery {
  /// Create a aurora device discovery that pings all enabled devices in the
  /// given [AuroraDevicesConfig].
  AuroraDevices(
      {required FeatureFlags featureFlags,
      required ProcessManager processManager,
      required Logger logger,
      required AuroraDevicesConfig config})
      : _auroraDeviceWorkflow = AuroraDeviceWorkflow(
          featureFlags: featureFlags,
        ),
        _logger = logger,
        _processManager = processManager,
        _config = config,
        super('aurora devices');

  final AuroraDeviceWorkflow _auroraDeviceWorkflow;
  final ProcessManager _processManager;
  final Logger _logger;
  final AuroraDevicesConfig _config;

  @override
  bool get supportsPlatform => true;

  @override
  bool get canListAnything => _auroraDeviceWorkflow.canListDevices;

  AuroraDevicesConfig get _auroraDevicesConfig => _config;

  Future<List<AuroraDevice>> get _enabledAuroraDevices async {
    final List<AuroraDevice> devices = _auroraDevicesConfig
        .tryGetDevices()
        .where((AuroraDeviceConfig element) => element.enabled)
        .map((AuroraDeviceConfig config) =>
            AuroraDevice(config: config, logger: _logger, processManager: _processManager))
        .toList();

    // Add emulator is exist and active
    final Map<String, dynamic>? info = await getEmulatorInfo();
    if (info != null) {
      devices.add(AuroraDevice(
        config: AuroraDeviceConfig(
          host: 'localhost',
          port: 2223,
          auth: '${info['sdkPath']}/vmshare/ssh/private_keys/sdk',
          rootPassword: null,
          platform: TargetPlatform.aurora_x64,
          version: info['version'].toString(),
          enabled: true,
          emulatorId: info['uuid'].toString(),
        ),
        logger: _logger,
        processManager: _processManager,
      ));
    }
    return devices;
  }

  // Only one emulator can be installed
  Future<Map<String, dynamic>?> getEmulatorInfo() async {
    final List<AuroraEmulator> emulators = await getEmulators();
    if (emulators.isEmpty) {
      return null;
    }
    final Map<String, dynamic>? info = await emulators.first.info;
    if (info == null || info['running'] == false) {
      return null;
    }
    return info;
  }

  @override
  Future<List<Device>> pollingGetDevices({Duration? timeout}) async {
    if (!canListAnything) {
      return const <Device>[];
    }

    final List<AuroraDevice> devices = await _enabledAuroraDevices;

    // maps any aurora device to whether its reachable or not.
    final Map<AuroraDevice, bool> pingedDevices = Map<AuroraDevice, bool>.fromIterables(
        devices, await Future.wait(devices.map((AuroraDevice e) => e.tryPing(timeout: timeout))));

    // remove all the devices we couldn't reach.
    pingedDevices.removeWhere((_, bool value) => !value);

    // return only the devices.
    return pingedDevices.keys.toList();
  }

  @override
  Future<List<String>> getDiagnostics() async => const <String>[];

  @override
  List<String> get wellKnownIds => const <String>[];
}
