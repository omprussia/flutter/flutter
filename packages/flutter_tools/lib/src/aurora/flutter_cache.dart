// SPDX-FileCopyrightText: Copyright 2023-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'dart:async';
import 'dart:convert';

import '../base/file_system.dart';
import '../base/os.dart' show OperatingSystemUtils;
import '../base/platform.dart';
import '../base/process.dart';
import '../cache.dart';
import '../globals.dart' as globals;
import 'platform/aurora_constants.dart';

/// A cached artifact Aurora Flutter-Embedder
class AuroraEmbedder extends CachedArtifact {
  AuroraEmbedder(Cache cache)
      : super(
          'aurora_embedder',
          cache,
          DevelopmentArtifact.universal,
        );

  @override
  Future<void> updateInner(
    ArtifactUpdater artifactUpdater,
    FileSystem fileSystem,
    OperatingSystemUtils operatingSystemUtils,
  ) async {
    // Get current branch name Flutter SDK
    final String branch = _getBranchName();
    // Clear folder embedder artifact
    await _clearCacheForGetLatestVersion();
    // Download
    return artifactUpdater.downloadZipArchive(
      'Downloading aurora_embedder tools...',
      _toStorageUri(
        branch: branch,
        latestVersion: branch == 'stable' ? await _getLatestVersion() : null,
      ),
      location,
    );
  }

  Uri _toStorageUri({required String branch, String? latestVersion}) {
    if (latestVersion == null || latestVersion.isEmpty) {
      const String mainUrl = 'https://gitlab.com/omprussia/flutter/flutter-embedder/-/archive/main/flutter-embedder-main.zip';
      final String branchUrl = 'https://gitlab.com/omprussia/flutter/flutter-embedder/-/archive/$branch/flutter-embedder-$branch.zip';
      return branch != 'main' && _checkUrlExist(branchUrl) ? Uri.parse(branchUrl) : Uri.parse(mainUrl);
    } else {
      return Uri.parse(
        'https://gitlab.com/omprussia/flutter/flutter-embedder/-/archive/$latestVersion/flutter-embedder-$latestVersion.zip',
      );
    }
  }

  Future<String?> _getLatestVersion() async {
    // Use tag only for stable version
    final String upstreamVersion = FRAMEWORK_VERSION.split('-').firstOrNull ?? FRAMEWORK_VERSION;
    final RunResult rawTags = await globals.processUtils.run(
      <String>[
        'curl',
        '--silent',
        '--fail',
        'https://gitlab.com/api/v4/projects/53351457/repository/tags?per_page=50&order_by=version&search=$upstreamVersion*'
      ],
    );
    if (rawTags.exitCode == 22) {
      return null;
    }
    final dynamic tag = (json.decode(rawTags.toString()) as List<dynamic>).firstOrNull;
    if (tag == null) {
      return null;
    }
    return (tag as Map<String, dynamic>)['name'].toString();
  }

  Future<void> _clearCacheForGetLatestVersion() async {
    final String artifactPath = globals.fs.path.join(
      globals.cache.getCacheArtifacts().path,
      'aurora_embedder',
    );
    await globals.fs.directory(artifactPath).delete(recursive: true);
  }
}

/// A cached artifact Aurora sign key
class AuroraSignKeys extends CachedArtifact {
  AuroraSignKeys(Cache cache)
      : super(
          'aurora_sign',
          cache,
          DevelopmentArtifact.universal,
        );

  final Uri openRegularKey = Uri.parse('https://developer.auroraos.ru/content-images/dev-doc/regular_key.pem');
  final Uri openRegularCert = Uri.parse('https://developer.auroraos.ru/content-images/dev-doc/regular_cert.pem');

  @override
  Future<void> updateInner(
    ArtifactUpdater artifactUpdater,
    FileSystem fileSystem,
    OperatingSystemUtils operatingSystemUtils,
  ) async {
    await artifactUpdater.downloadFile(
      'Downloading sign package key...',
      openRegularKey,
      location,
    );
    await artifactUpdater.downloadFile(
      'Downloading sign package cert...',
      openRegularCert,
      location,
    );
  }
}

/// Artifacts required for desktop Aurora builds.
class AuroraEngineArtifacts extends EngineCachedArtifact {
  AuroraEngineArtifacts(Cache cache, {required Platform platform})
      : _platform = platform,
        super(
          'aurora-sdk',
          cache,
          DevelopmentArtifact.aurora,
        );

  final Platform _platform;

  @override
  List<String> getPackageDirs() => const <String>[];

  @override
  List<List<String>> getBinaryDirs() {
    if (_platform.isLinux || ignorePlatformFiltering) {
      // Get current branch name Flutter SDK
      final String branch = _getBranchName();
      return <List<String>>[
        <String>[
          'aurora',
          _toStorageUri(
            branch: branch,
            latestVersion: branch == 'stable' ? FRAMEWORK_VERSION : null,
          ),
        ],
      ];
    }
    return const <List<String>>[];
  }

  String _toStorageUri({required String branch, String? latestVersion}) {
    if (latestVersion == null || latestVersion.isEmpty) {
      const String mainUrl = 'https://gitlab.com/omprussia/flutter/flutter-engine/-/archive/main/flutter-engine-main.zip';
      final String branchUrl = 'https://gitlab.com/omprussia/flutter/flutter-engine/-/archive/$branch/flutter-engine-$branch.zip';
      return branch != 'main' && _checkUrlExist(branchUrl) ? branchUrl : mainUrl;
    } else {
      return 'https://gitlab.com/omprussia/flutter/flutter-engine/-/archive/aurora-$latestVersion/flutter-engine-aurora-$latestVersion.zip';
    }
  }

  @override
  List<String> getLicenseDirs() => const <String>[];
}

String _getBranchName() {
  final RunResult runResult = globals.processUtils.runSync(
    <String>['git', 'rev-parse', '--abbrev-ref', 'HEAD'],
    workingDirectory: Cache.flutterRoot,
  );
  return runResult.stdout.trim();
}

bool _checkUrlExist(String url) {
  final RunResult runResult = globals.processUtils.runSync(
    <String>[ 'curl', '--head', '--fail', url],
  );
  return runResult.exitCode == 0;
}
