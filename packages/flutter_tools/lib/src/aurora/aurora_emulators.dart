// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'dart:io';

import '../base/common.dart';
import '../base/process.dart';
import '../device.dart';
import '../emulator.dart';
import '../globals.dart' as globals;

const String vBoxManage = '/usr/lib/virtualbox/VBoxManage';

class AuroraEmulators extends EmulatorDiscovery {
  @override
  bool get supportsPlatform => globals.platform.isLinux;

  @override
  bool get canListAnything => true;

  @override
  Future<List<Emulator>> get emulators async => getEmulators();

  @override
  bool get canLaunchAnything => canListAnything;
}

class AuroraEmulator extends Emulator {
  const AuroraEmulator(String id) : super(id, true);

  @override
  String get name => 'Emulator Aurora OS';

  @override
  String get manufacturer => 'Open Mobile Platform';

  @override
  Category get category => Category.mobile;

  @override
  PlatformType get platformType => PlatformType.aurora;

  Future<Map<String, dynamic>?> get info async {
    try {
      final RunResult result = await globals.processUtils.run(
        <String>[vBoxManage, 'showvminfo', id],
      );
      final Map<String, dynamic> info = <String, dynamic>{};
      for (final String line in result.stdout.split('\n')) {
        if (line.contains('Config file:') && !info.containsKey('sdkPath')) {
          info['sdkPath'] = line.split(' ').lastOrNull?.split('/emulator').firstOrNull;
        }
        if (line.contains('UUID:') && !info.containsKey('uuid')) {
          info['uuid'] = line.split(' ').lastOrNull;
        }
        if (line.contains('Name:') && !info.containsKey('version')) {
          info['version'] = line.split(' ').lastOrNull.toString().replaceAll('AuroraOS-', '').replaceAll('-base', '');
        }
        if (line.contains('State:') && !info.containsKey('running')) {
          info['running'] = line.contains('running');
        }
      }
      return info;
    } on Exception catch (_) {}
    return null;
  }

  @override
  Future<void> launch({bool coldBoot = false}) async {
      if (coldBoot) {
      await _execVBoxManage(
        <String>['snapshot', id, 'delete'],
      );
    }
    final RunResult runningResult = await _execVBoxManage(
      <String>['list', 'runningvms'],
    );
    if (!runningResult.stdout.contains(id)) {
      final RunResult launchResult = await _execVBoxManage(
        <String>['startvm', id],
      );
      if (launchResult.exitCode != 0) {
        throwToolExit('Failed to start the emulator.');
      }
    }
  }

  Future<void> takeScreenshot(File outputFile) async {
    final RunResult result = await _execVBoxManage(<String>[
      'controlvm',
      id,
      'screenshotpng',
      outputFile.path,
    ]);
    if (result.exitCode != 0) {
      final String message = result.stderr.isNotEmpty ? result.stderr : result.stdout;
      if (message.isNotEmpty) {
        result.throwException('Taking screenshot error:\n$message');
      }
      result.throwException('Taking screenshot failed without a message');
    }
  }

  Future<RunResult> _execVBoxManage(List<String> command) {
    try {
      return globals.processUtils.run(<String>[vBoxManage] + command);
    } on Exception catch (_) {
      throwToolExit('Error execute command: `${command.join(' ')}`');
    }
  }
}

/// Return the list of Aurora Emulators.
Future<List<AuroraEmulator>> getEmulators() async {
  final List<AuroraEmulator> emulators = <AuroraEmulator>[];
  try {
    final RunResult result = await globals.processUtils.run(
      <String>[vBoxManage, 'list', 'vms'],
    );
    final List<String> ids = result.stdout
        .split('\n')
        .where((String element) => element.contains('AuroraOS'))
        .map((String line) => line.split(' ')[1].replaceAll(RegExp(r'[{}]'), ''))
        .toList();
    for (final String id in ids) {
      final AuroraEmulator emulator = AuroraEmulator(id);
      final Map<String, dynamic>? info = await emulator.info;
      if (info != null && info['version'].toString().startsWith('5.')) {
        emulators.add(AuroraEmulator(id));
      }
    }
  } on Exception catch (_) {}
  return emulators;
}
