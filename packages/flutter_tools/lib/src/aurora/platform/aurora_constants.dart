// SPDX-FileCopyrightText: Copyright 2023-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import '../../build_info.dart';

//////////////////////
// Flutter

// @todo if not upstream
/// Version Flutter SDK
const String FRAMEWORK_VERSION = '3.27.1';


//////////////////////
// Architectures

/// List architectures Aurora 4
const Map<TargetPlatform, String> ARCHITECTURES_4 = <TargetPlatform, String>{
  TargetPlatform.aurora_arm: 'armv7hl',
};

/// List architectures Aurora 5
const Map<TargetPlatform, String> ARCHITECTURES_5 = <TargetPlatform, String>{
  TargetPlatform.aurora_arm: 'armv7hl',
  TargetPlatform.aurora_arm64: 'aarch64',
  TargetPlatform.aurora_x64: 'x86_64',
};

//////////////////////
// Errors

/// Show error if PSDK_DIR not set flutter config
const String ERROR_PSDK_CONF = r'''
Necessary to add the path to psdk_dir to the flutter configuration.
Example: `flutter config --aurora-psdk-dir=$HOME/AuroraPlatformSDK/sdks/aurora_psdk`
More information about Platform SDK: https://developer.auroraos.ru/doc/software_development/psdk''';

/// Show error in doctor if not found PSDK chroot tool
const String ERROR_PSDK_TOOL = '''
Failed to access Platform SDK.
The Platform SDK is required for assembly; more details about the installation can be found here:
https://developer.auroraos.ru/doc/software_development/psdk/setup''';

/// Show error in doctor if not found PSDK targets
const String ERROR_PSDK_TARGETS = '''
No targets found in Platform SDK. Check their names, they should match the pattern:
AuroraOS-{version}-base-{architecture}
Platform SDK setup: https://developer.auroraos.ru/doc/software_development/psdk/setup''';

/// Show error in doctor if not found sdk-chroot sudoers fix
const String ERROR_PSDK_SUDOERS = r'''
Executing the command requires root access.
Update your sudoers settings using the template:

Update file /etc/sudoers.d/mer-sdk-chroot:

{user} ALL=(ALL) NOPASSWD: {psdk_dir}
Defaults!{psdk_dir} env_keep += "SSH_AGENT_PID SSH_AUTH_SOCK"

Update file /etc/sudoers.d/sdk-chroot:

{user} ALL=(ALL) NOPASSWD: {psdk_dir}/sdk-chroot
Defaults!{psdk_dir}/sdk-chroot env_keep += "SSH_AGENT_PID SSH_AUTH_SOCK"''';

/// Show warning not supported sudoers
const String WARNING_PSDK_SUDOERS_NOT_SUPPORT = r'''
Attention: working with sudoers for Platform SDK is supported only for Ubuntu!''';

/// Show error in doctor if not found PSDK target
const String ERROR_PSDK_TARGET = '''
Target {arch} is not available.''';

/// Sudoers sdk-chroot path
const String PSDK_SUDOERS_PATH = '/etc/sudoers.d/sdk-chroot';

/// Sudoers sdk-chroot data
const String PSDK_SUDOERS = r'''
{user} ALL=(ALL) NOPASSWD: {psdk_dir}/sdk-chroot
Defaults!{psdk_dir}/sdk-chroot env_keep += "SSH_AGENT_PID SSH_AUTH_SOCK"
''';

/// Sudoers mer-sdk-chroot path
const String PSDK_SUDOERS_MER_PATH = '/etc/sudoers.d/mer-sdk-chroot';

/// Sudoers mer-sdk-chroot data
const String PSDK_SUDOERS_MER = r'''
{user} ALL=(ALL) NOPASSWD: {psdk_dir}
Defaults!{psdk_dir} env_keep += "SSH_AGENT_PID SSH_AUTH_SOCK"
''';
