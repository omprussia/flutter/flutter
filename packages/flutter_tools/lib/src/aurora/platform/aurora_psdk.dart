// SPDX-FileCopyrightText: Copyright 2023-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'dart:convert';
import 'dart:io';

import '../../base/common.dart';
import '../../base/process.dart';
import '../../build_info.dart';
import '../../globals.dart' as globals;
import 'aurora_constants.dart';
import 'aurora_extensions.dart';

class AuroraPSDK {
  AuroraPSDK._(this._tool);

  final String _tool;
  static String? _version;

  static Future<AuroraPSDK> getInstance({String? psdkDir}) async {
    // Get from flutter config
    if (psdkDir == null) {
      final Object? psdkDirConf = globals.config.getValue('aurora-psdk-dir');
      if (psdkDirConf != null) {
        psdkDir = psdkDirConf.toString();
      } else {
        throw Exception(ERROR_PSDK_CONF);
      }
    }
    // Get and check tool
    final String tool = '$psdkDir/sdk-chroot';
    if (!await globals.fs.file(tool).exists()) {
      throw Exception(ERROR_PSDK_TOOL);
    }
    // Check sudoers activate
    if (AuroraPSDK.isUbuntu() && !await AuroraPSDK.isSudoers(psdkDir)) {
        throw Exception(ERROR_PSDK_SUDOERS.format(<String, String>{
          'user': Platform.environment['USER'] ?? r'$USER',
          'psdk_dir': psdkDir,
        }));
    }
    // Create instance
    final AuroraPSDK psdk = AuroraPSDK._(tool);
    // init static version
    _version ??= await psdk.getVersion();
    // result
    return psdk;
  }

  static bool isUbuntu() {
    try {
      return File('/etc/os-release').readAsLinesSync().contains('ID=ubuntu');
    } on Exception catch(e) {
      globals.printWarning(e.toString());
      return false;
    }
  }

  static String getStaticVersion() {
    if (_version == null) {
      throwToolExit(
        'Platform SDK is not initialized. Please check your Platform SDK sudo access.',
      );
    }
    return _version!;
  }

  static String getStaticVersionMajor() {
    return getStaticVersion().substring(0, 1);
  }

  static Future<bool> addSudoers(String psdkDir) async {
    final Map<String, String> data = <String, String>{
      PSDK_SUDOERS_PATH: PSDK_SUDOERS,
      PSDK_SUDOERS_MER_PATH: PSDK_SUDOERS_MER,
    };
    for (final MapEntry<String, String> item in data.entries) {
      final File file = File(item.key);
      final bool isFileExist = await globals.fs.file(file.path).exists();
      final bool isAddSudoers;
      if (isFileExist) {
        final List<String> fileLines = await file.openRead().map(utf8.decode).transform(const LineSplitter()).toList();
        isAddSudoers = fileLines.firstWhere((String line) => line.contains(psdkDir), orElse: () => '').isEmpty;
      } else {
        isAddSudoers = true;
      }
      if (isAddSudoers) {
        try {
          if (!isFileExist) {
            await globals.processUtils.run(
              <String>['sudo', 'touch', file.path],
            );
          }
          final RunResult result = await globals.processUtils.run(
            <String>['sudo', 'chmod', '777', file.path],
          );
          if (result.exitCode != 0) {
            return false;
          }
          await file.writeAsString(mode: FileMode.append, PSDK_SUDOERS.format(<String, String>{
            'user': Platform.environment['USER']!,
            'psdk_dir': psdkDir,
          }));
          await globals.processUtils.run(
            <String>['sudo', 'chmod', '644', file.path],
          );
          await globals.processUtils.run(
            <String>['sudo', 'chown', 'root:root', file.path],
          );
        } on Exception catch (_) {
          return false;
        }
      }
    }
    return true;
  }

  static Future<bool> isSudoers(String psdkDir) async {
    if (!AuroraPSDK.isUbuntu()) {
      return false;
    }
    for (final String path in <String>[PSDK_SUDOERS_PATH, PSDK_SUDOERS_MER_PATH]) {
      final bool isExist = await globals.fs.file(path).exists();
      if (!isExist) {
        return false;
      }
      if ((await File(path).openRead().map(utf8.decode).transform(const LineSplitter()).toList())
          .firstWhere((String line) => line.contains(psdkDir), orElse: () => '')
          .isEmpty) {
        return false;
      }
    }
    return true;
  }

  Future<String?> getVersion() async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        _tool,
        'version',
      ],
    );
    return const LineSplitter().convert(result.stdout).toList().lastOrNull?.split(' ').elementAt(1);
  }

  Future<List<String>?> getListTargets() async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        _tool,
        'sb2-config',
        '-f',
      ],
    );
    return const LineSplitter().convert(result.stdout);
  }

  Future<String?> findTargetName(TargetPlatform targetPlatform) async {
    final List<String>? targetsNames = await getListTargets();
    if (targetsNames == null) {
      return null;
    }

    final String? arch = _getArchMap()?[targetPlatform];
    if (arch == null) {
      return null;
    }

    final List<String> psdkTargets =
        targetsNames.where((String e) => e.contains('Aurora') && e.contains(arch) && !e.contains('default')).toList();

    return psdkTargets.firstOrNull;
  }

  Future<bool> buildRPM(
    String path,
    BuildInfo buildInfo,
    TargetPlatform targetPlatform,
  ) async {
    final String version = getStaticVersion();
    final String versionMajor = getStaticVersionMajor();
    final String? targetName = await findTargetName(targetPlatform);

    if (targetName == null) {
      throwToolExit('Not found target PSDK');
    }

    final int result = await globals.processUtils.stream(
      <String>[
        _tool,
        'mb2',
        '--target',
        targetName,
        '--no-fix-version',
        'build',
        if (buildInfo.mode == BuildMode.debug) '-d',
        path,
        '--',
        '--define',
        '_flutter_psdk_version $version',
        '--define',
        '_flutter_psdk_major ${int.parse(versionMajor)}',
        '--define',
        if (buildInfo.mode == BuildMode.debug) '_flutter_build_type Debug',
        if (buildInfo.mode != BuildMode.debug) '_flutter_build_type Release',
      ],
      workingDirectory: getAuroraBuildDirectory(targetPlatform, buildInfo),
      treatStderrAsStdout: true,
    );

    return result == 0;
  }

  Future<bool> trySignPackages(List<String> paths, {int timeout = 3}) async {
    final Directory pathKeys = Directory(globals.fs.path.join(
      globals.cache.getCacheArtifacts().path,
      'aurora_sign',
    ));
    final String pathKey = globals.fs.path.join(
      pathKeys.path,
      'regular_key.pem',
    );
    final String pathCert = globals.fs.path.join(
      pathKeys.path,
      'regular_cert.pem',
    );
    if (await globals.fs.file(pathKey).exists() && await globals.fs.file(pathCert).exists()) {
      for (final String path in paths) {
        // Remove old sign
        await globals.processUtils.run(
          <String>[_tool, 'rpmsign-external', 'delete', path],
        );
        // Add new sign
        final RunResult result = await globals.processUtils.run(
          <String>[_tool, 'rpmsign-external', 'sign', '--key=$pathKey', '--cert=$pathCert', path],
        );
        if (result.exitCode != 0) {
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }

  Future<bool> checkEmbedder(
    TargetPlatform targetPlatform,
  ) async {
    // Get folder embedder RPM
    final Directory? embedderArtifact = await getPathEmbedder(targetPlatform);
    if (embedderArtifact == null) {
      return false;
    }
    // Get PSDK target name
    final String? targetName = await findTargetName(targetPlatform);
    if (targetName == null) {
      return false;
    }
    // Get version install embedder
    final String? installVersion = await getVersionEmbedder(targetName);
    // Get list rpm packages
    final List<FileSystemEntity> rpms = await embedderArtifact.list().toList();
    // Get version folder embedder
    final String folderVersion = globals.fs.path
        .basename(rpms.first.path)
        .replaceAll('.${_getArchMap()?[targetPlatform]}.rpm', '')
        .replaceAll('flutter-embedder-', '')
        .replaceAll('devel-', '');

    // Install embedder
    if (installVersion == null || folderVersion != installVersion && !installVersion.contains('-dev')) {
      globals.printStatus(
        '${installVersion == null ? 'Install' : 'Reinstall'} flutter embedder to target "$targetName"...',
      );
      await removeEmbedder(targetName);
      final List<String> packages = rpms.map((FileSystemEntity e) => e.path).toList();
      packages.sort();
      for (final String path in packages) {
        if (!await installToTargetRPM(targetName, path)) {
          return false;
        }
      }
    }
    return true;
  }

  Future<Directory?> getPathEmbedder(
    TargetPlatform targetPlatform,
  ) async {
    final String? arch = _getArchMap()?[targetPlatform];

    if (arch == null) {
      return null;
    }

    final Directory pathEmbedders = Directory(globals.fs.path.join(
      globals.cache.getCacheArtifacts().path,
      'aurora_embedder',
    ));

    final List<String> folder = (await pathEmbedders.list().toList())
        .map((FileSystemEntity entity) => globals.fs.path.basename(entity.path))
        .toList();

    return Directory(globals.fs.path.join(
      globals.cache.getCacheArtifacts().path,
      'aurora_embedder',
      folder.first,
      'embedder',
      'psdk_${AuroraPSDK.getStaticVersionMajor()}',
      arch,
    ));
  }

  Future<bool> installToTargetRPM(String targetName, String pathRPM) async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        _tool,
        'sb2',
        '-t',
        targetName,
        '-m',
        'sdk-install',
        '-R',
        'zypper',
        '--no-gpg-checks',
        'in',
        '-y',
        pathRPM,
      ],
    );
    return result.exitCode == 0;
  }

  Future<bool> removeEmbedder(String targetName) async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        _tool,
        'sb2',
        '-t',
        targetName,
        '-m',
        'sdk-install',
        '-R',
        'zypper',
        'rm',
        '-y',
        'flutter-embedder',
      ],
    );
    return result.exitCode == 0;
  }

  Future<String?> getVersionEmbedder(String target) async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        _tool,
        'sb2',
        '-t',
        target,
        '-R',
        'zypper',
        '--disable-repositories',
        'search',
        '--installed-only',
        '-s',
        'flutter',
      ],
    );
    return result.stdout
        .split('\n')
        .where((String e) => e.contains('flutter-embedder-devel'))
        .firstOrNull
        ?.split('|')[3]
        .trim();
  }

  List<TargetPlatform>? getArchPlatforms() {
    return _getArchMap()?.keys.toList();
  }

  List<String>? getArchNames() {
    return _getArchMap()?.values.toList();
  }

  Map<TargetPlatform, String>? _getArchMap() {
    return getStaticVersionMajor() == '5' ? ARCHITECTURES_5 : ARCHITECTURES_4;
  }
}
