// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'dart:async';

import 'package:async/async.dart';
import 'package:meta/meta.dart';

import '../../base/common.dart';
import '../../base/error_handling_io.dart';
import '../../base/file_system.dart';
import '../../base/io.dart';
import '../../base/logger.dart';
import '../../base/platform.dart';
import '../../base/process.dart';
import '../../base/terminal.dart';
import '../../build_info.dart';
import '../../convert.dart';
import '../../features.dart';
import '../../globals.dart' as globals;
import '../../runner/flutter_command.dart';
import '../../runner/flutter_command_runner.dart';
import '../aurora_devices/aurora_device_config.dart';
import '../aurora_devices/aurora_devices_config.dart';

/// just the function signature of the [print] function.
/// The Object arg may be null.
typedef PrintFn = void Function(Object);

class AuroraDevicesCommand extends FlutterCommand {
  factory AuroraDevicesCommand({
    required AuroraDevicesConfig auroraDevicesConfig,
    required Terminal terminal,
    required Platform platform,
    required FileSystem fileSystem,
    required Logger logger,
    required FeatureFlags featureFlags,
  }) {
    return AuroraDevicesCommand._common(
        auroraDevicesConfig: auroraDevicesConfig,
        terminal: terminal,
        platform: platform,
        fileSystem: fileSystem,
        logger: logger,
        featureFlags: featureFlags);
  }

  @visibleForTesting
  factory AuroraDevicesCommand.test(
      {required AuroraDevicesConfig auroraDevicesConfig,
      required Terminal terminal,
      required Platform platform,
      required FileSystem fileSystem,
      required Logger logger,
      required FeatureFlags featureFlags,
      PrintFn usagePrintFn = print}) {
    return AuroraDevicesCommand._common(
        auroraDevicesConfig: auroraDevicesConfig,
        terminal: terminal,
        platform: platform,
        fileSystem: fileSystem,
        logger: logger,
        featureFlags: featureFlags,
        usagePrintFn: usagePrintFn);
  }

  AuroraDevicesCommand._common({
    required AuroraDevicesConfig auroraDevicesConfig,
    required Terminal terminal,
    required Platform platform,
    required FileSystem fileSystem,
    required Logger logger,
    required FeatureFlags featureFlags,
    PrintFn usagePrintFn = print,
  })  : _auroraDevicesConfig = auroraDevicesConfig,
        _usagePrintFn = usagePrintFn {
    addSubcommand(AuroraDevicesListCommand(
      auroraDevicesConfig: auroraDevicesConfig,
      featureFlags: featureFlags,
      logger: logger,
    ));
    addSubcommand(AuroraDevicesResetCommand(
      auroraDevicesConfig: auroraDevicesConfig,
      featureFlags: featureFlags,
      fileSystem: fileSystem,
      logger: logger,
    ));
    addSubcommand(AuroraDevicesAddCommand(
      auroraDevicesConfig: auroraDevicesConfig,
      terminal: terminal,
      platform: platform,
      featureFlags: featureFlags,
      fileSystem: fileSystem,
      logger: logger,
    ));
    addSubcommand(AuroraDevicesDeleteCommand(
      auroraDevicesConfig: auroraDevicesConfig,
      featureFlags: featureFlags,
      fileSystem: fileSystem,
      logger: logger,
    ));
  }

  final AuroraDevicesConfig _auroraDevicesConfig;
  final PrintFn _usagePrintFn;

  @override
  String get description {
    String configFileLine;
    configFileLine = '\nMakes changes to the config file at "${_auroraDevicesConfig.configPath}".\n';

    return '''
List, reset, register and delete Aurora OS devices.
$configFileLine
Aurora OS devices are registered via Flutter CLI.
Complete a short survey to register your devices.
''';
  }

  @override
  String get name => 'aurora-devices';

  @override
  String get category => FlutterCommandCategory.tools;

  @override
  Future<FlutterCommandResult> runCommand() async {
    return FlutterCommandResult.success();
  }

  @override
  void printUsage() {
    _usagePrintFn(usage);
  }
}

/// This class is meant to provide some commonly used utility functions
/// to the subcommands, like backing up the config file & checking if the
/// feature is enabled.
abstract class AuroraDevicesCommandBase extends FlutterCommand {
  AuroraDevicesCommandBase({
    required this.auroraDevicesConfig,
    required this.featureFlags,
    required this.fileSystem,
    required this.logger,
  });

  @protected
  final AuroraDevicesConfig auroraDevicesConfig;
  @protected
  final FeatureFlags featureFlags;
  @protected
  final FileSystem? fileSystem;
  @protected
  final Logger logger;

  /// The path to the (potentially non-existing) backup of the config file.
  @protected
  String get configBackupPath => '${auroraDevicesConfig.configPath}.bak';

  /// Copies the current config file to [configBackupPath], overwriting it
  /// if necessary. Returns false and does nothing if the current config file
  /// doesn't exist. (True otherwise)
  @protected
  bool backup() {
    final File configFile = fileSystem!.file(auroraDevicesConfig.configPath);
    if (configFile.existsSync()) {
      configFile.copySync(configBackupPath);
      return true;
    }
    return false;
  }

  /// Checks if the aurora devices feature is enabled and returns true/false
  /// accordingly. Additionally, logs an error if it's not enabled with a hint
  /// on how to enable it.
  @protected
  void checkFeatureEnabled() {}
}

class AuroraDevicesListCommand extends AuroraDevicesCommandBase {
  AuroraDevicesListCommand({
    required super.auroraDevicesConfig,
    required super.featureFlags,
    required super.logger,
  }) : super(fileSystem: null);

  @override
  String get description => '''
List currently configured Aurora OS devices, both enabled and disabled, reachable or not.
''';

  @override
  String get name => 'list';

  @override
  Future<FlutterCommandResult> runCommand() async {
    checkFeatureEnabled();

    late List<AuroraDeviceConfig> devices;
    try {
      devices = auroraDevicesConfig.devices;
    } on Exception {
      throwToolExit('Could not list aurora devices.');
    }

    if (devices.isEmpty) {
      logger.printStatus('No aurora devices found in "${auroraDevicesConfig.configPath}"');
    } else {
      logger.printStatus('List of Aurora OS devices in "${auroraDevicesConfig.configPath}":');
      for (final AuroraDeviceConfig device in devices) {
        logger.printStatus(
          'id: ${device.id}, label: ${device.label}, enabled: ${device.enabled}',
          indent: 2,
          hangingIndent: 2,
        );
      }
    }

    return FlutterCommandResult.success();
  }
}

class AuroraDevicesResetCommand extends AuroraDevicesCommandBase {
  AuroraDevicesResetCommand({
    required super.auroraDevicesConfig,
    required super.featureFlags,
    required FileSystem super.fileSystem,
    required super.logger,
  });

  @override
  String get description => '''
Reset the Aurora OS devices config file to defaults.

The current config file will be backed up to the same path, but with a `.bak` appended.
If a file already exists at the backup location, it will be overwritten.
''';

  @override
  String get name => 'reset';

  @override
  Future<FlutterCommandResult> runCommand() async {
    checkFeatureEnabled();

    final bool wasBackedUp = backup();

    ErrorHandlingFileSystem.deleteIfExists(fileSystem!.file(auroraDevicesConfig.configPath));
    auroraDevicesConfig.ensureFileExists();

    logger.printStatus(wasBackedUp
        ? 'Successfully reset the Aurora OS devices config file and created a '
            'backup at "$configBackupPath".'
        : 'Successfully reset the Aurora OS devices config file.');
    return FlutterCommandResult.success();
  }
}

class AuroraDevicesAddCommand extends AuroraDevicesCommandBase {
  AuroraDevicesAddCommand({
    required super.auroraDevicesConfig,
    required Terminal terminal,
    required Platform platform,
    required super.featureFlags,
    required FileSystem super.fileSystem,
    required super.logger,
  })  : _terminal = terminal,
        _platform = platform {
    argParser.addFlag(_kCheck,
        help: 'Make sure the config actually works. This will execute some of the '
            'commands in the config (if necessary with dummy arguments). This '
            'flag is enabled by default when "--json" is not specified. If '
            '"--json" is given, it is disabled by default.\n'
            'For example, a config with "null" as the "runDebug" command is '
            'invalid. If the "runDebug" command is valid (so it is an array of '
            'strings) but the command is not found (because you have a typo, for '
            'example), the config won\'t work and "--check" will spot that.');

    argParser.addOption(_kJson,
        help: 'Add the aurora device described by this JSON-encoded string to the '
            'list of aurora-devices instead of using the normal, interactive way '
            'of configuring. Useful if you want to use the "flutter aurora-devices '
            'add" command from a script, or use it non-interactively for some '
            'other reason.\n'
            "By default, this won't check whether the passed in config actually "
            'works. For more info see the "--check" option.',
        valueHelp: '{"id": "pi", ...}',
        aliases: _kJsonAliases);

    argParser.addFlag(_kSsh,
        help: 'Add a ssh-device. This will automatically fill out some of the config '
            'options for you with good defaults, and in other cases save you some '
            "typing. So you'll only need to enter some things like hostname and "
            'username of the remote device instead of entering each individual '
            'command.',
        defaultsTo: true,
        negatable: false);
  }

  static const String _kJson = 'json';
  static const List<String> _kJsonAliases = <String>['js'];
  static const String _kCheck = 'check';
  static const String _kSsh = 'ssh';

  final Terminal _terminal;
  final Platform _platform;
  late StreamQueue<String> inputs;

  @override
  String get description => 'Add a new device to the Aurora OS devices config file.';

  @override
  String get name => 'add';

  void _printConfigCheckingError(String err) {
    logger.printError(err);
  }

  Future<bool> _tryPing(String host, String port, {int timeout = 3}) async {
    final RunResult result = await globals.processUtils.run(
      <String>['nc', '-w', timeout.toString(), '-vz', host, port],
    );
    return result.exitCode == 0;
  }

  Future<String?> _tryGetTargetPlatform(String ip, String port, String key, {int timeout = 3}) async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        'ssh',
        '-o',
        'ConnectTimeout=$timeout',
        '-i',
        key,
        'defaultuser@$ip',
        '-p$port',
        '-t',
        'cat /etc/rpm/platform',
      ],
    );
    if (result.exitCode == 0) {
      if (result.stdout.contains('armv7hl')) {
        return 'aurora-arm';
      } else if (result.stdout.contains('aarch64')) {
        return 'aurora-arm64';
      }
    }
    return null;
  }

  Future<String?> _tryGetVersion(String ip, String port, String key, {int timeout = 3}) async {
    final RunResult result = await globals.processUtils.run(
      <String>[
        'ssh',
        '-o',
        'ConnectTimeout=$timeout',
        '-i',
        key,
        'defaultuser@$ip',
        '-p$port',
        '-t',
        'cat /etc/os-release',
      ],
    );
    if (result.exitCode == 0) {
      for (final String line in result.stdout.split('\n')) {
        if (line.contains('VERSION_ID=')) {
          return line.split('=').lastOrNull;
        }
      }
    }
    return null;
  }

  /// Run non-interactively (useful if running from scripts or bots),
  /// add value of the `--json` arg to the config.
  ///
  /// Only check if `--check` is explicitly specified. (Don't check by default)
  Future<FlutterCommandResult> runNonInteractively() async {
    final String jsonStr = stringArg(_kJson)!;
    final bool shouldCheck = boolArg(_kCheck);

    dynamic json;
    try {
      json = jsonDecode(jsonStr);
    } on FormatException catch (e) {
      throwToolExit('Could not decode json: $e');
    }

    late AuroraDeviceConfig config;
    try {
      config = AuroraDeviceConfig.fromJson(json);
      if (!await _isUniqueKey('host', config.host)) {
        throwToolExit('A device with the same IP has already been added: ${config.host}');
      }
    } on AuroraDeviceRevivalException catch (e) {
      throwToolExit('Invalid aurora device config: $e');
    }

    if (shouldCheck && !await _tryPing(config.host, config.port.toString())) {
      throwToolExit("Custom device didn't pass all checks.");
    }

    auroraDevicesConfig.add(config);
    printSuccessfullyAdded();

    return FlutterCommandResult.success();
  }

  void printSuccessfullyAdded() {
    logger.printStatus('Successfully added Aurora OS device to config file at "${auroraDevicesConfig.configPath}".');
  }

  bool isValidIp(String s) => InternetAddress.tryParse(s) != null;

  Future<bool> _isFileExist(String s) => globals.fs.file(s).exists();

  Future<bool> _isUniqueKey(String key, String value) async {
    if (!await _isFileExist(auroraDevicesConfig.configPath)) {
      return true;
    }
    final String data = await globals.fs.file(auroraDevicesConfig.configPath).readAsString();
    return !data.contains('"$key": "$value"');
  }

  /// Ask the user to input a string.
  Future<String?> askForString(
    String name, {
    bool? isUnique,
    String? description,
    String? example,
    String? defaultsTo,
    Future<bool> Function(String)? validator,
  }) async {
    String msg = description ?? name;

    final String exampleOrDefault = <String>[
      if (example != null) 'example: $example',
      if (defaultsTo != null) 'empty for $defaultsTo',
    ].join(', ');

    if (exampleOrDefault.isNotEmpty) {
      msg += ' ($exampleOrDefault)';
    }

    logger.printStatus(msg);
    while (true) {
      if (!await inputs.hasNext) {
        return null;
      }

      final String input = await inputs.next;

      if (validator != null && !await validator(input)) {
        if (isUnique != null && isUnique) {
          logger.printStatus('Invalid input. Please enter unique $name:');
        } else {
          logger.printStatus('Invalid input. Please enter $name:');
        }
      } else {
        return input;
      }
    }
  }

  /// Ask the user for a y(es) / n(o) or empty input.
  Future<bool> askForBool(
    String name, {
    String? description,
    bool defaultsTo = true,
  }) async {
    final String defaultsToStr = defaultsTo ? '[Y/n]' : '[y/N]';
    logger.printStatus('$description $defaultsToStr (empty for default)');
    while (true) {
      final String input = await inputs.next;

      if (input.isEmpty) {
        return defaultsTo;
      } else if (input.toLowerCase() == 'y') {
        return true;
      } else if (input.toLowerCase() == 'n') {
        return false;
      } else {
        logger.printStatus('Invalid input. Expected is either y, n or empty for default. $name? $defaultsToStr');
      }
    }
  }

  /// Ask the user if he wants to apply the config.
  /// Shows a different prompt if errors or warnings exist in the config.
  Future<bool> askApplyConfig({bool hasErrorsOrWarnings = false}) {
    return askForBool('apply',
        description: hasErrorsOrWarnings
            ? 'Warnings or errors exist in Aurora OS device. '
                'Would you like to add the Aurora OS device to the config anyway?'
            : 'Would you like to add the Aurora OS device to the config now?',
        defaultsTo: !hasErrorsOrWarnings);
  }

  /// Run interactively (with user prompts), the target device should be
  /// connected to via ssh.
  Future<FlutterCommandResult> runInteractivelySsh() async {
    final bool shouldCheck = boolArg(_kCheck);

    if (!_platform.isLinux) {
      throw UnsupportedError('Unsupported operating system');
    }

    // Listen to the keystrokes stream as late as possible, since it's a
    // single-subscription stream apparently.
    // Also, _terminal.keystrokes can be closed unexpectedly, which will result
    // in StreamQueue.next throwing a StateError when make the StreamQueue listen
    // to that directly.
    // This caused errors when using Ctrl+C to terminate while the
    // aurora-devices add command is waiting for user input.
    // So instead, we add the keystrokes stream events to a new single-subscription
    // stream and listen to that instead.
    final StreamController<String> nonClosingKeystrokes = StreamController<String>();
    final StreamSubscription<String> keystrokesSubscription =
        _terminal.keystrokes.listen((String s) => nonClosingKeystrokes.add(s.trim()), cancelOnError: true);

    inputs = StreamQueue<String>(nonClosingKeystrokes.stream);

    final String host = (await askForString(
      'ip',
      isUnique: true,
      description: 'Please enter the IP address of your Aurora OS device. You can get it from Settings -> Developer Tools.',
      example: '192.168.2.15',
      validator: (String s) async {
        if (isValidIp(s) && await _isUniqueKey('host', s)) {
          return true;
        }
        return false;
      },
    ))!;

    final String port = (await askForString(
      'port',
      description: 'Please enter the device port, the default is 22.',
      example: '22',
      validator: (String s) async => RegExp(r'^\d+$').hasMatch(s),
    ))!;

    final bool pingConnect = await _tryPing(host, port);
    if (shouldCheck && !pingConnect) {
      _printConfigCheckingError("Couldn't ping device.");
      exit(1);
    }

    final String auth = (await askForString(
      'ssh-key',
      description: 'Please enter the path to the SSH private key used to authorized on the Aurora OS device.',
      example: '/home/user/.ssh/id_rsa',
      validator: (String s) async {
        if (RegExp(r'^(.+)/([^/]+)$').hasMatch(s) && await _isFileExist(s)) {
          return true;
        }
        return false;
      },
    ))!;

    String? targetPlatform;
    String? version;

    if (pingConnect) {
      targetPlatform = await _tryGetTargetPlatform(host, port, auth);
      if (shouldCheck && targetPlatform == null) {
        _printConfigCheckingError("Couldn't get target platform from device.");
        exit(1);
      }
      version = await _tryGetVersion(host, port, auth);
      if (shouldCheck && version == null) {
        _printConfigCheckingError("Couldn't get version from device.");
        exit(1);
      }
    }

    final String rootPassword = (await askForString(
      'devel-su',
      description: 'Please enter the devel-su password needed to install applications via pkcon.',
      example: '00000',
      validator: (String s) async => RegExp(r'^\w+$').hasMatch(s),
    ))!;

    targetPlatform ??= (await askForString(
      'target-platform',
      description: 'Please enter the target Aurora OS device platform (aurora-arm, aurora-arm64, aurora-x64).',
      example: 'aurora-arm',
      validator: (String s) async => RegExp(r'^(aurora-arm|aurora-arm64|aurora-x64)$').hasMatch(s),
    ))!;

    version ??= (await askForString(
      'device-version',
      description: 'Please enter the device version: Settings -> About device -> Build.',
      example: '4.0.2.303',
      validator: (String s) async => RegExp(r'^\d+.\d+.\d+.\d+$').hasMatch(s),
    ))!;

    final AuroraDeviceConfig config = AuroraDeviceConfig(
      host: host,
      port: int.parse(port),
      auth: auth,
      rootPassword: rootPassword,
      platform: getTargetPlatformForName(targetPlatform),
      version: version,
      enabled: true,
    );

    final bool apply = await askApplyConfig();

    unawaited(keystrokesSubscription.cancel());
    unawaited(nonClosingKeystrokes.close());

    if (apply) {
      auroraDevicesConfig.add(config);
      printSuccessfullyAdded();
    }

    return FlutterCommandResult.success();
  }

  @override
  Future<FlutterCommandResult> runCommand() async {
    checkFeatureEnabled();

    if (stringArg(_kJson) != null) {
      return runNonInteractively();
    }
    if (boolArg(_kSsh)) {
      return runInteractivelySsh();
    }
    throw UnsupportedError('Unknown run mode');
  }
}

class AuroraDevicesDeleteCommand extends AuroraDevicesCommandBase {
  AuroraDevicesDeleteCommand({
    required super.auroraDevicesConfig,
    required super.featureFlags,
    required FileSystem super.fileSystem,
    required super.logger,
  });

  @override
  String get description => '''
Delete a device from the Aurora OS devices config file.
''';

  @override
  String get name => 'delete';

  @override
  Future<FlutterCommandResult> runCommand() async {
    checkFeatureEnabled();

    final String? id = globalResults![FlutterGlobalOptions.kDeviceIdOption] as String?;
    if (id == null || !auroraDevicesConfig.contains(id)) {
      throwToolExit('Couldn\'t find device with id "$id" in config at "${auroraDevicesConfig.configPath}"');
    }

    backup();
    auroraDevicesConfig.remove(id);
    logger.printStatus('Successfully removed device with id "$id" from config at "${auroraDevicesConfig.configPath}"');
    return FlutterCommandResult.success();
  }
}
