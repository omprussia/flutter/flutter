// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import '../application_package.dart';
import '../base/file_system.dart';
import '../build_info.dart';
import '../cmake_project.dart';

abstract class AuroraApp extends ApplicationPackage {
  AuroraApp({
    required String projectBundleId,
    required this.buildInfo,
  }) : super(id: projectBundleId);

  /// Creates a new [AuroraApp] from a linux sub project.
  factory AuroraApp.fromAuroraProject(AuroraProject project, BuildInfo? buildInfo) {
    return BuildableAuroraApp(
      project: project,
      buildInfo: buildInfo,
    );
  }

  /// Creates a new [AuroraApp] from an existing executable.
  ///
  /// `applicationBinary` is the path to the executable.
  factory AuroraApp.fromPrebuiltApp(FileSystemEntity applicationBinary) {
    return PrebuiltAuroraApp(
      executable: applicationBinary.path,
      buildInfo: null
    );
  }

  @override
  String get displayName => id;

  final BuildInfo? buildInfo;

  String executable(BuildMode buildMode);
}

class PrebuiltAuroraApp extends AuroraApp {
  PrebuiltAuroraApp({
    required String executable,
    required super.buildInfo,
  })  : _executable = executable,
        super(projectBundleId: executable);

  final String _executable;

  @override
  String executable(BuildMode buildMode) => _executable;

  @override
  String get name => _executable;
}

class BuildableAuroraApp extends AuroraApp {
  BuildableAuroraApp({required this.project, super.buildInfo})
      : super(projectBundleId: project.parent.manifest.appName);

  final AuroraProject project;

  String? getPackageName(CmakeBasedProject project) {
    final Directory dirSpec = project.parent.directory.childDirectory('aurora').childDirectory('rpm');
    if (!dirSpec.existsSync()) {
      return null;
    }
    final FileSystemEntity? fileSpec = dirSpec.listSync().firstOrNull;
    if (fileSpec == null) {
      return null;
    }
    for (final String line in (fileSpec as File).readAsLinesSync()) {
      if (line.contains('Name: ')) {
        final List<String> items = line.split(' ');
        if (items.length > 1) {
          return items[1];
        }
      }
    }
    return null;
  }

  @override
  String executable(BuildMode buildMode) {
    return '/usr/bin/${getPackageName(project)}';
  }

  @override
  String get name => project.parent.manifest.appName;
}
