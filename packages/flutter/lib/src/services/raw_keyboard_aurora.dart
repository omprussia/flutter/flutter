// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

/// @docImport 'hardware_keyboard.dart';
library;

import 'raw_keyboard.dart';

/// Platform-specific key event data for Aurora OS.
///
/// See also:
///
/// * [RawKeyboard], which uses this interface to expose key data.
class RawKeyEventDataAurora extends RawKeyEventData {

  /// Creates a key event data structure specific for Aurora OS.
  const RawKeyEventDataAurora({required this.physicalKey, required this.logicalKey});

  @override
  final PhysicalKeyboardKey physicalKey;

  @override
  final LogicalKeyboardKey logicalKey;

  @override
  KeyboardSide? getModifierSide(ModifierKey key) => null;

  @override
  bool isModifierPressed(ModifierKey key, {KeyboardSide side = KeyboardSide.any}) => false;

  @override
  String get keyLabel => '';
}
