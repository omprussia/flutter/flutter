/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <flutter/flutter_aurora.h>
#include <flutter/flutter_aurora_capi.h>

#include "generated_plugin_registrant.h"

int main(int argc, char* argv[]) {
    aurora::Initialize(argc, argv);
    aurora::RegisterPlugins();

    /**
     * Set entry point using environment variable if available,
     * otherwise set entry point using command line parameters
     * from desktop file if specified or select main entry point.
     */
    FlutterAuroraLaunchOptions options;
    FlutterAuroraLaunchOptionsInitDefault(&options, sizeof(FlutterAuroraLaunchOptions));
    options.dart_entry_point = std::getenv("DART_ENTRY_POINT");

    FlutterAuroraLaunch(&options);
    return 0;
}
