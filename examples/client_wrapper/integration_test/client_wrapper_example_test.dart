// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>

import 'package:client_wrapper/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('| Client Wrapper Example Integration Tests |', () {
    testWidgets('**Check app startup and UI display**',
        (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder appBarTitleFinder = find.byKey(const Key('appBarTitle'));
      expect(appBarTitleFinder, findsOneWidget);

      final Finder clientWrapperFinder = find.text('Client Wrapper');
      expect(clientWrapperFinder, findsOneWidget);

      final Finder listItemInfoFinder = find.byKey(const Key('listItemInfo'));
      expect(listItemInfoFinder, findsOneWidget);

      final Finder textureRegistarFinder = find.text('Texture Registar');
      expect(textureRegistarFinder, findsOneWidget);

      final Finder eventChannelFinder = find.text('Event Channel');
      expect(eventChannelFinder, findsOneWidget);

      final Finder binaryMessengerFinder = find.text('Binary Messenger');
      expect(binaryMessengerFinder, findsOneWidget);

      final Finder encodableValueFinder = find.text('Encodable Value');
      expect(encodableValueFinder, findsOneWidget);
    });

    testWidgets('**Checking the display of values from the Event Channel**',
        (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      // Wait for Stream update
      await tester.pump(const Duration(seconds: 2));

      // Check text value presence for Event Channel
      final Finder eventChannelValueKeyFinder =
          find.byKey(const Key('eventChannelValue'));
      expect(eventChannelValueKeyFinder, findsWidgets);
      expect(find.text('0°'), findsAny);
    });

    testWidgets('**Checking the display of values from Binary Messenger**',
        (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      // Wait for Stream update
      await tester.pump(const Duration(seconds: 2));

      // Check text value presence for Binary Messenger
      final Finder binaryMessengerValueKeyFinder =
          find.byKey(const Key('binaryMessengerValue'));
      expect(binaryMessengerValueKeyFinder, findsWidgets);
      expect(find.text('0°'), findsAny);
    });
  });
  testWidgets('**Checking the display of the Encoded Value table**',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MyApp());
    await tester.pumpAndSettle();

    // Wait for Stream update
    await tester.pump(const Duration(seconds: 2));

    // Checking for a DataTable
    final Finder encodableDataTableFinder =
        find.byKey(const Key('encodableDataTable'));
    expect(encodableDataTableFinder, findsOneWidget);

    // Checking for rows with expected data
    final Finder encodableTypeIntFinder =
        find.byKey(const Key('encodableType_int'));
    expect(encodableTypeIntFinder, findsOneWidget);
    expect(find.text('int'), findsOneWidget);
    final Finder encodableValueIntFinder =
        find.byKey(const Key('encodableValue_int'));
    expect(encodableValueIntFinder, findsOneWidget);
    expect(find.text('1'), findsOneWidget);

    final Finder encodableTypeBoolFinder =
        find.byKey(const Key('encodableType_bool'));
    expect(encodableTypeBoolFinder, findsOneWidget);
    expect(find.text('bool'), findsOneWidget);
    final Finder encodableValueBoolFinder =
        find.byKey(const Key('encodableValue_bool'));
    expect(encodableValueBoolFinder, findsOneWidget);
    expect(find.text('true'), findsOneWidget);

    final Finder encodableTypeStringFinder =
        find.byKey(const Key('encodableType_String'));
    expect(encodableTypeStringFinder, findsOneWidget);
    expect(find.text('String'), findsOneWidget);
    final Finder encodableValueStringFinder =
        find.byKey(const Key('encodableValue_String'));
    expect(encodableValueStringFinder, findsOneWidget);
    expect(find.text('text'), findsOneWidget);

    final Finder encodableTypeUnmodifiableInt32ArrayViewFinder =
        find.byKey(const Key('encodableType__UnmodifiableInt32ArrayView'));
    expect(encodableTypeUnmodifiableInt32ArrayViewFinder, findsOneWidget);
    expect(find.text('Int32Array'), findsOneWidget);
    final Finder encodableValueUnmodifiableInt32ArrayViewFinder =
        find.byKey(const Key('encodableValue__UnmodifiableInt32ArrayView'));
    expect(encodableValueUnmodifiableInt32ArrayViewFinder, findsOneWidget);
    expect(find.text('[1, 2]'), findsOneWidget);

    final Finder encodableTypeUnmodifiableFloat64ArrayViewFinder =
        find.byKey(const Key('encodableType__UnmodifiableFloat64ArrayView'));
    expect(encodableTypeUnmodifiableFloat64ArrayViewFinder, findsOneWidget);
    expect(find.text('Float64Array'), findsOneWidget);
    final Finder encodableValueUnmodifiableFloat64ArrayViewFinder =
        find.byKey(const Key('encodableValue__UnmodifiableFloat64ArrayView'));
    expect(encodableValueUnmodifiableFloat64ArrayViewFinder, findsOneWidget);
    expect(find.text('[1.0, 2.0]'), findsOneWidget);

    final Finder encodableTypeMapFinder =
        find.byKey(const Key('encodableType__Map<Object?, Object?>'));
    expect(encodableTypeMapFinder, findsOneWidget);
    expect(find.text('Map'), findsOneWidget);
    final Finder encodableValueMapFinder =
        find.byKey(const Key('encodableValue__Map<Object?, Object?>'));
    expect(encodableValueMapFinder, findsOneWidget);
    expect(find.text('{key: value}'), findsOneWidget);
  });
}
