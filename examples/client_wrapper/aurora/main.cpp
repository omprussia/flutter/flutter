/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <client_wrapper/client_wrapper_plugin.h>
#include <flutter/flutter_aurora.h>

int main(int argc, char* argv[]) {
    aurora::Initialize(argc, argv);
    ClientWrapperPlugin::RegisterWithRegistrar(aurora::GetPluginRegistrar());  // Register plugin
    aurora::Launch();
    return 0;
}
