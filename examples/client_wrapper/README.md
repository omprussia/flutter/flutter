# Platform Channels & Client Wrapper

Demonstrates how to use the Platform Channels with Client Wrapper for Aurora OS.

You can read more about
[accessing platform and third-party services in Flutter](https://flutter.dev/to/platform-channels/).

You can read more about
[Client Wrapper](https://omprussia.gitlab.io/flutter/flutter/structure/plugins/#client-wrapper).

## Aurora OS

You can use the commands `flutter build` and `flutter run` from the app's root
directory to build/run the app.
