// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
// ignore_for_file: always_specify_types

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

// Platform plugin keys channels
const String channelEvent = 'client_wrapper_event';
const String channelMethods = 'client_wrapper_methods';
const String channelMessageBinary = 'client_wrapper_binary';

// Platform channel plugin methods
enum Methods {
  createTexture,
  eventChannelEnable,
  eventChannelDisable,
  binaryMessengerEnable,
  binaryMessengerDisable,
  encodable,
}

/// An implementation of [ClientWrapperPlatform] that uses method channels.
class MethodChannelClientWrapper extends ClientWrapperPlatform {
  /// The methods channel used to interact with the native platform.
  final EventChannel eventChannel = const EventChannel(channelEvent);
  final MethodChannel methodsChannel = const MethodChannel(channelMethods);
  final MethodChannel methodsChannelBinary = const MethodChannel(channelMessageBinary);

  /// Create texture with default image
  /// Return texture ID
  @override
  Future<int?> createTexture() {
    return methodsChannel.invokeMethod<int?>(Methods.createTexture.name);
  }

  /// EncodableValue to transfer data from
  /// flutter platform channels to dart
  @override
  Future<dynamic> encodable(Map<String, dynamic> values) {
    return methodsChannel.invokeMethod<Object?>(
        Methods.encodable.name, values);
  }

  /// Scream screen orientation angle with EventChannel
  @override
  Stream<int?> listenEventChannel() {
    return eventChannel.receiveBroadcastStream().map((event) => event as int);
  }

  /// Scream screen orientation angle with BinaryMessage
  @override
  Stream<int?> eventBinaryMessage() {
    // Init controller for enable/disable event
    final StreamController<int?> streamController = StreamController<int?>(
      onPause: () => methodsChannelBinary
          .invokeMethod<Object?>(Methods.binaryMessengerDisable.name),
      onResume: () => methodsChannelBinary
          .invokeMethod<Object?>(Methods.binaryMessengerEnable.name),
      onCancel: () => methodsChannelBinary
          .invokeMethod<Object?>(Methods.binaryMessengerDisable.name),
      onListen: () => methodsChannelBinary
          .invokeMethod<Object?>(Methods.binaryMessengerEnable.name),
    );
    // Add listen handler
    const OptionalMethodChannel(channelMessageBinary, JSONMethodCodec())
        .setMethodCallHandler((MethodCall call) {
      if (call.method == 'ChangeDisplayOrientation') {
        streamController.add(int.parse(call.arguments.toString()));
      }
      return Future<void>.value();
    });
    // Return stream
    return streamController.stream;
  }
}

abstract class ClientWrapperPlatform extends PlatformInterface {
  /// Constructs a ClientWrapperPlatform.
  ClientWrapperPlatform() : super(token: _token);

  static final Object _token = Object();

  static ClientWrapperPlatform _instance = MethodChannelClientWrapper();

  /// The default instance of [ClientWrapperPlatform] to use.
  ///
  /// Defaults to [MethodChannelCameraAurora].
  static ClientWrapperPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [ClientWrapperPlatform] when
  /// they register themselves.
  @visibleForTesting
  static set instance(ClientWrapperPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  /// Create texture with default image
  /// Return texture ID
  Future<int?> createTexture();

  /// EncodableValue to transfer data from
  /// flutter platform channels to dart
  Future<dynamic> encodable(Map<String, dynamic> values) {
    throw UnimplementedError('encodable() has not been implemented.');
  }

  /// Scream screen orientation angle with EventChannel
  Stream<int?> listenEventChannel() {
    throw UnimplementedError('listenEventChannel() has not been implemented.');
  }

  /// Scream screen orientation angle with BinaryMessage
  Stream<int?> eventBinaryMessage() {
    throw UnimplementedError('eventBinaryMessage() has not been implemented.');
  }
}

class ClientWrapper {
  /// Create texture with default image
  /// Return texture ID
  Future<int?> createTexture() =>
      ClientWrapperPlatform.instance.createTexture();

  /// EncodableValue to transfer data from
  /// flutter platform channels to dart
  Future<dynamic> encodable(Map<String, dynamic> values) =>
      ClientWrapperPlatform.instance.encodable(values);

  /// Scream screen orientation angle with EventChannel
  Stream<int?> listenEventChannel() =>
      ClientWrapperPlatform.instance.listenEventChannel();

  /// Scream screen orientation angle with BinaryMessage
  Stream<int?> eventBinaryMessage() =>
      ClientWrapperPlatform.instance.eventBinaryMessage();
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final ClientWrapper _plugin = ClientWrapper();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Client Wrapper',
            key: ValueKey<String>('appBarTitle'),
          ),
        ),
        body: ListSeparated(
          children: <Widget>[
            const ListItemInfo(
              '''
              This is a plugin if the main goal is to demonstrate working
              with Flutter Embedder through the common client_wrapper.
              ''',
              key: ValueKey<String>('listItemInfo'),
            ),
            ListItemData<int?>(
              'Texture Registar',
              InternalColors.pink,
              description: '''
              The image is drawn using the GPU, textures and pixel buffer
              through the common client_wrapper.
              ''',
              widthData: 100,
              future: _plugin.createTexture(),
              builder: (int? value) {
                if (value != null) {
                  return SizedBox(
                    width: 84,
                    height: 84,
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Texture(
                          textureId: value,
                          key: ValueKey<String>('texture_$value'),
                        ),
                      ),
                    ),
                  );
                }
                return const SizedBox.shrink();
              },
              key: const ValueKey<String>('listItemData_texture'),
            ),
            ListItemData<int?>(
              'Event Channel',
              InternalColors.orange,
              description: '''
              Implementing an event using the flutter::EventChannel
              client_wrapper to obtain the screen orientation angle.
              ''',
              widthData: 100,
              stream: _plugin.listenEventChannel(),
              builder: (int? value) => value == null
                  ? const SizedBox.shrink()
                  : Text(
                      '$value°',
                      key: const ValueKey<String>('eventChannelValue'),
                    ),
              key: const ValueKey<String>('listItemData_eventChannel'),
            ),
            ListItemData<int?>(
              'Binary Messenger',
              InternalColors.green,
              description: '''
              Implementing an event using the BinaryMessenger client_wrapper
              to obtain the screen orientation angle.
              ''',
              widthData: 100,
              stream: _plugin.eventBinaryMessage(),
              builder: (int? value) => value == null
                  ? const SizedBox.shrink()
                  : Text(
                      '$value°',
                      key: const ValueKey<String>('binaryMessengerValue'),
                    ),
              key: const ValueKey<String>('listItemData_binaryMessenger'),
            ),
            ListItemData(
              'Encodable Value',
              InternalColors.purple,
              description: '''
              Example of using EncodableValue to transfer data from
              flutter platform channels to dart.
              ''',
              future: _plugin.encodable(<String, dynamic>{
                'int': 1,
                'bool': true,
                'string': 'text',
                'vector_int': <int>[1, 2],
                'vector_double': <double>[1.0, 2.0],
                'map': <String, String>{'key': 'value'}
              }),
              builder: (value) {
                if (value != null) {
                  final List<DataRow> rows = <DataRow>[];
                  if (value is List) {
                    for (final item in value) {
                      rows.add(
                        DataRow(
                          cells: <DataCell>[
                            DataCell(
                              Text(
                                item.runtimeType
                                    .toString()
                                    .replaceAll('_', '')
                                    .replaceAll('View', '')
                                    .replaceAll('Unmodifiable', '')
                                    .replaceAll('<Object?, Object?>', ''),
                                key: ValueKey<String>(
                                    'encodableType_${item.runtimeType}'),
                              ),
                            ),
                            DataCell(
                              Text(
                                item.toString(),
                                key: ValueKey<String>(
                                    'encodableValue_${item.runtimeType}'),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                  }
                  return SizedBox(
                    width: double.infinity,
                    child: Card(
                      child: DataTable(
                        key: const ValueKey<String>('encodableDataTable'),
                        horizontalMargin: 16,
                        columnSpacing: 6,
                        columns: const <DataColumn>[
                          DataColumn(
                            label: Expanded(
                              child: Text(
                                'Type',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                          ),
                          DataColumn(
                            label: Expanded(
                              child: Text(
                                'Value',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                          ),
                        ],
                        rows: rows,
                      ),
                    ),
                  );
                }
                return const SizedBox.shrink();
              },
              key: const ValueKey<String>('listItemData_encodable'),
            ),
          ],
        ),
      ),
    );
  }
}
