/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <platform_channel/platform_channel_plugin.h>

#include <iostream>

namespace Channels {
constexpr auto Event = "samples.flutter.io/charging";
constexpr auto Methods = "samples.flutter.io/battery";
}  // namespace Channels

namespace Methods {
constexpr auto BatteryLevel = "getBatteryLevel";
}  // namespace Methods

void PlatformChannelPlugin::RegisterWithRegistrar(flutter::PluginRegistrar* registrar) {
    // Create MethodChannel with StandardMethodCodec
    auto methodChannel = std::make_unique<MethodChannel>(registrar->messenger(), Channels::Methods,
                                                         &flutter::StandardMethodCodec::GetInstance());

    // Create EventChannel with StandardMethodCodec
    auto eventChannel = std::make_unique<EventChannel>(registrar->messenger(), Channels::Event,
                                                       &flutter::StandardMethodCodec::GetInstance());

    // Create plugin
    std::unique_ptr<PlatformChannelPlugin> plugin(
        new PlatformChannelPlugin(std::move(methodChannel), std::move(eventChannel)));

    // Register plugin
    registrar->AddPlugin(std::move(plugin));
}

PlatformChannelPlugin::PlatformChannelPlugin(std::unique_ptr<MethodChannel> methodChannel,
                                             std::unique_ptr<EventChannel> eventChannel)
    : m_methodChannel(std::move(methodChannel)), m_eventChannel(std::move(eventChannel)) {
    // Create MethodHandler
    RegisterMethodHandler();
    // Create StreamHandler
    RegisterStreamHandler();
}

void PlatformChannelPlugin::RegisterMethodHandler() {
    m_methodChannel->SetMethodCallHandler([this](const MethodCall& call, std::unique_ptr<MethodResult> result) {
        if (call.method_name().compare(Methods::BatteryLevel) == 0) {
            result->Success(onGetBatteryLevel());
        } else {
            result->Success();
        }
    });
}

void PlatformChannelPlugin::RegisterStreamHandler() {
    auto handler = std::make_unique<flutter::StreamHandlerFunctions<EncodableValue>>(
        [&](const EncodableValue* arguments, std::unique_ptr<flutter::EventSink<EncodableValue>>&& events)
            -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>> {
            m_sink = std::move(events);
            onEventChannelEnable();
            return nullptr;
        },
        [&](const EncodableValue* arguments) -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>> {
            onEventChannelDisable();
            return nullptr;
        });

    m_eventChannel->SetStreamHandler(std::move(handler));
}

// ========== event_channel ==========

void PlatformChannelPlugin::onEventChannelSend(std::string state) {
    // Send data to EventChannel
    m_sink->Success(state);
}

void PlatformChannelPlugin::onEventChannelEnable() {
    // Enable listen
    m_stateEventChannel = true;
    // Send random sate to event for demonstration
    onEventChannelSend((1 + std::rand() % 2 == 1) ? "charging" : "discharging");
}

void PlatformChannelPlugin::onEventChannelDisable() {
    m_stateEventChannel = false;
}

// ========== method_channel ==========

EncodableValue PlatformChannelPlugin::onGetBatteryLevel() {
    // Random level for for demonstration
    return 1 + std::rand() % 99;
}
