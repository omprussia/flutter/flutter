/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#ifndef FLUTTER_PLUGIN_PLATFORM_CHANNEL_PLUGIN_H
#define FLUTTER_PLUGIN_PLATFORM_CHANNEL_PLUGIN_H

#include <platform_channel/globals.h>

#include <flutter/encodable_value.h>
#include <flutter/event_channel.h>
#include <flutter/event_stream_handler_functions.h>
#include <flutter/method_channel.h>
#include <flutter/plugin_registrar.h>
#include <flutter/standard_method_codec.h>

// Flutter encodable
typedef flutter::EncodableValue EncodableValue;
typedef flutter::EncodableMap EncodableMap;
typedef flutter::EncodableList EncodableList;
// Flutter methods
typedef flutter::MethodChannel<EncodableValue> MethodChannel;
typedef flutter::MethodCall<EncodableValue> MethodCall;
typedef flutter::MethodResult<EncodableValue> MethodResult;
// Flutter events
typedef flutter::EventChannel<EncodableValue> EventChannel;
typedef flutter::EventSink<EncodableValue> EventSink;

class PLUGIN_EXPORT PlatformChannelPlugin final : public flutter::Plugin {
public:
    static void RegisterWithRegistrar(flutter::PluginRegistrar* registrar);

private:
    // Creates a plugin that communicates on the given channel.
    PlatformChannelPlugin(std::unique_ptr<MethodChannel> methodChannel, std::unique_ptr<EventChannel> eventChannel);

    // Methods register handlers channels
    void RegisterMethodHandler();
    void RegisterStreamHandler();

    // Methods MethodCall
    EncodableValue onGetBatteryLevel();

    // Methods EventChannel
    void onEventChannelSend(std::string state);
    void onEventChannelEnable();
    void onEventChannelDisable();

    // Variables
    std::unique_ptr<MethodChannel> m_methodChannel;
    std::unique_ptr<EventChannel> m_eventChannel;
    std::unique_ptr<EventSink> m_sink;
    bool m_stateEventChannel = false;
};

#endif /* FLUTTER_PLUGIN_PLATFORM_CHANNEL_PLUGIN_H */
