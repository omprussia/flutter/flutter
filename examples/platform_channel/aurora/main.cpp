/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <flutter/flutter_aurora.h>
#include <platform_channel/platform_channel_plugin.h>
#include "generated_plugin_registrant.h"

int main(int argc, char* argv[]) {
    aurora::Initialize(argc, argv);
    aurora::RegisterPlugins();
    PlatformChannelPlugin::RegisterWithRegistrar(aurora::GetPluginRegistrar());  // Register plugin
    aurora::Launch();
    return 0;
}
