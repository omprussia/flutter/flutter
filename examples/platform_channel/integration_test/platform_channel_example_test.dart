// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>

import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:platform_channel/main.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('| Platform channel Example Integration Tests |', () {
    testWidgets('**Check app startup and UI display**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder levelFinder = find.textContaining('Battery level');
      expect(levelFinder, findsOne);

      final Finder statusFinder = find.textContaining('Battery status');
      expect(statusFinder, findsOne);
    });

    testWidgets('**Test for PlatformChannel Battery Level**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder batteryLevelUnknownFinder = find.text('Battery level: unknown.');
      expect(batteryLevelUnknownFinder, findsOne);

      final Finder refreshButtonFinder = find.text('Refresh');
      await tester.tap(refreshButtonFinder);

      await tester.pumpAndSettle();

      final Finder batteryLevelDataFinder = find.textContaining('%');
      expect(batteryLevelDataFinder, findsOne);
    });

    testWidgets('**Test for PlatformChannel Battery Status**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder batteryStatusFinder = find.textContaining('Battery status');
      expect(batteryStatusFinder, findsOne);

      final Finder batteryStatusUnknownFinder = find.text('Battery status: unknown.');
      expect(batteryStatusUnknownFinder, findsNothing);
    });
  });
}
