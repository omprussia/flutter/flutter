# Platform Channels & Qt

Demonstrates how to use the Platform Channels with Qt on Aurora OS.

You can read more about
[accessing platform and third-party services in Flutter](https://flutter.dev/to/platform-channels/).

You can read more about
[example](https://omprussia.gitlab.io/flutter/flutter/examples/platform-channels-qt/).

## Aurora OS

You can use the commands `flutter build` and `flutter run` from the app's root
directory to build/run the app.
