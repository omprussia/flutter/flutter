/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <platform_channels_qt/platform_channels_qt_plugin.h>

namespace Channels {
constexpr auto Event = "platform_channels_qt";
}  // namespace Channels

void PlatformChannelsQtPlugin::RegisterWithRegistrar(flutter::PluginRegistrar* registrar) {
    // Create EventChannel with StandardMethodCodec
    auto eventChannel = std::make_unique<EventChannel>(registrar->messenger(), Channels::Event,
                                                       &flutter::StandardMethodCodec::GetInstance());

    // Create plugin
    std::unique_ptr<PlatformChannelsQtPlugin> plugin(new PlatformChannelsQtPlugin(std::move(eventChannel)));

    // Register plugin
    registrar->AddPlugin(std::move(plugin));
}

PlatformChannelsQtPlugin::PlatformChannelsQtPlugin(std::unique_ptr<EventChannel> eventChannel)
    : m_eventChannel(std::move(eventChannel)), m_state(m_manager.isOnline()) {
    // Create StreamHandler
    RegisterStreamHandler();
}

void PlatformChannelsQtPlugin::RegisterStreamHandler() {
    // Create handler for events Platform Channels
    auto handler = std::make_unique<flutter::StreamHandlerFunctions<EncodableValue>>(
        [&](const EncodableValue*, std::unique_ptr<flutter::EventSink<EncodableValue>>&& events)
            -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>> {
            m_sink = std::move(events);
            onEventChannelEnable();
            return nullptr;
        },
        [&](const EncodableValue*) -> std::unique_ptr<flutter::StreamHandlerError<EncodableValue>> {
            onEventChannelDisable();
            return nullptr;
        });

    // Register event
    m_eventChannel->SetStreamHandler(std::move(handler));
}

void PlatformChannelsQtPlugin::onEventChannelEnable() {
    // Send after start
    m_sink->Success(m_state);
    // Connect listen connection
    m_onlineStateChangedConnection = QObject::connect(&m_manager, &QNetworkConfigurationManager::onlineStateChanged,
                                                      this, &PlatformChannelsQtPlugin::onEventChannelSend);
}

void PlatformChannelsQtPlugin::onEventChannelDisable() {
    // Disconnect listen connection
    QObject::disconnect(m_onlineStateChangedConnection);
}

void PlatformChannelsQtPlugin::onEventChannelSend() {
    // Send state is change
    auto state = m_manager.isOnline();
    if (state != m_state) {
        m_state = state;
        m_sink->Success(m_state);
    }
}

// Add moc file
#include "moc_platform_channels_qt_plugin.cpp"
