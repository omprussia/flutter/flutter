/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <flutter/flutter_aurora.h>
#include <flutter/flutter_compatibility_qt.h>  // Add for Qt
#include <platform_channels_qt/platform_channels_qt_plugin.h>

int main(int argc, char* argv[]) {
    aurora::Initialize(argc, argv);
    aurora::EnableQtCompatibility();                                                // Enable Qt
    PlatformChannelsQtPlugin::RegisterWithRegistrar(aurora::GetPluginRegistrar());  // Register plugin
    aurora::Launch();
    return 0;
}
