// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:platform_channels_qt/main.dart';

void main() {
  group('| Platform Channel QT Integration Tests |', () {
    testWidgets('**Check app startup and UI display**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder appBarFinder = find.textContaining('Platform Channels Qt');
      expect(appBarFinder, findsOne);
    });

    testWidgets('**Test platform channel qt stateNetworkConnect method**', (WidgetTester tester) async {
      bool? result;
      final Stream<bool?> subscription = PlatformChannelsQt().stateNetworkConnect();
      final Completer<void> completer = Completer<void>();

      subscription.listen((bool? event) {
        result = event;
        completer.complete();
      });
      await completer.future;
      expect(result, isNotNull);

      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder resultFinder = find.text(result.toString());
      expect(resultFinder, findsOneWidget);
    });
  });
}
