// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

const String channelEvent = 'platform_channels_qt';

/// An implementation of [PlatformChannelsQtPlatform] that uses method channels.
class MethodChannelPlatformChannelsQt extends PlatformChannelsQtPlatform {
  /// The method channel used to interact with the native platform.
  final EventChannel eventChannel = const EventChannel(channelEvent);

  /// Scream network state with EventChannel
  @override
  Stream<bool?> stateNetworkConnect() {
    return eventChannel.receiveBroadcastStream().map((dynamic event) => event as bool);
  }
}

abstract class PlatformChannelsQtPlatform extends PlatformInterface {
  /// Constructs a PlatformChannelsQtPlatform.
  PlatformChannelsQtPlatform() : super(token: _token);

  static final Object _token = Object();

  static PlatformChannelsQtPlatform _instance = MethodChannelPlatformChannelsQt();

  /// The default instance of [PlatformChannelsQtPlatform] to use.
  ///
  /// Defaults to [MethodChannelPlatformChannelsQt].
  static PlatformChannelsQtPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [PlatformChannelsQtPlatform] when
  /// they register themselves.
  @visibleForTesting
  static set instance(PlatformChannelsQtPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Stream<bool?> stateNetworkConnect() {
    throw UnimplementedError('connectNetworkState() has not been implemented.');
  }
}

class PlatformChannelsQt {
  Stream<bool?> stateNetworkConnect() {
    return PlatformChannelsQtPlatform.instance.stateNetworkConnect();
  }
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final PlatformChannelsQt _plugin = PlatformChannelsQt();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Platform Channels Qt'),
        ),
        body: ListSeparated(
          children: <Widget>[
            const ListItemInfo('''
            An example of a platform-dependent plugin that works with the
            Aurora OS via the Platform Channels and Qt signal/slot.
            '''),
            ListItemData<bool?>(
              'State network connect',
              InternalColors.purple,
              description: 'Listen status network connect through Platform Channels with use Qt',
              stream: _plugin.stateNetworkConnect(),
            ),
          ],
        ),
      ),
    );
  }
}
