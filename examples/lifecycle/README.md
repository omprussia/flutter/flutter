# Lifecycle

Demonstrates how to use the Lifecycle on Aurora OS.

You can read more about
[AppLifecycleListener](https://api.flutter.dev/flutter/widgets/AppLifecycleListener-class.html).

## Aurora OS

You can use the commands `flutter build` and `flutter run` from the app's root
directory to build/run the app.
