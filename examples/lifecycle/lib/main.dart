// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';

void main() {
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final AppLifecycleListener _listener;

  late AppLifecycleState? _state;

  @override
  void initState() {
    super.initState();
    _state = SchedulerBinding.instance.lifecycleState;
    _listener = AppLifecycleListener(
      onStateChange: _handleStateChange,
    );
  }

  void _handleStateChange(AppLifecycleState state) {
    setState(() {
      _state = state;
      debugPrint('$_state');
    });
  }

  @override
  void dispose() {
    _listener.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: Scaffold(
          appBar: AppBar(
            title: const Text('App lifecycle demo'),
          ),
          body: ListSeparated(
            children: <Widget>[
              const ListItemInfo('''
              This demo shows the lifecycle of a flutter application.
              '''),
              ListItemData<AppLifecycleState>(
                'App lifecycle states',
                description: 'This is where the application lifecycle states are displayed',
                InternalColors.green,
                value: _state,
              ),
            ],
          ),
        ),
    );
  }
}
