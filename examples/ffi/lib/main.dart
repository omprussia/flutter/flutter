// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'dart:ffi';
import 'package:ffi/ffi.dart';

import 'package:flutter/material.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';

import 'device_info_bindings_generated.dart';

class FFI {
  /// Bindings to native functions in `libffi.so`.
  final DeviceInfoBindings _bindings = DeviceInfoBindings(
    DynamicLibrary.open('libdevice_info.so'),
  );

  /// Implementation of a method to obtain the device name.
  String? getDeviceName() => using((Arena arena) {
        // Method Execution
        final Pointer<Utf8> pointer = _bindings.getDeviceName().cast<Utf8>();
        // Free memory
        arena.using(pointer, calloc.free);
        // Returning the result
        final String deviceName = pointer.toDartString();
        if (deviceName == '') {
          return null;
        }
        return deviceName;
      });
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FFI _plugin = FFI();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Foreign function interface',
            key: ValueKey<String>('appBarTitle'),
          ),
        ),
        body: ListSeparated(
          children: <Widget>[
            const ListItemInfo('''
            An example of a platform-dependent plugin that works with the
            Aurora OS via the FFI.
            '''),
            ListItemData<String?>(
              'Get Device Name',
              InternalColors.purple,
              description: 'Getting the device name through a platform-dependent plugin FFI via D-Bus',
              value: _plugin.getDeviceName(),
            ),
          ],
        ),
      ),
    );
  }
}
