# Foreign function interface

Demonstrates how to use the FFI on Aurora OS.

You can read more about
[C interop](https://dart.dev/interop/c-interop).

## Aurora OS

You can use the commands `flutter build` and `flutter run` from the app's root
directory to build/run the app.

Install libclangdev if not installed:
With apt-get: `sudo apt-get install libclang-dev`.
With dnf: `sudo dnf install clang-devel`.

Generate bindings: `flutter pub run ffigen --config ffigen.yaml`.
