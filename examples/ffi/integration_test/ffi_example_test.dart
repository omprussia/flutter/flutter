// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>

import 'package:ffi_example/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('| FFI Example Integration Tests |', () {
    testWidgets('**Check app startup and UI display**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder addBarFinder = find.byKey(const Key('appBarTitle'));
      expect(addBarFinder, findsOneWidget);
    });

    testWidgets('**Test FFI getDevice method**', (WidgetTester tester) async {
      final FFI plugin = FFI();

      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final String? deviceName = plugin.getDeviceName();
      if (deviceName == null) {
        fail('FFI getDeviceName method failed');
      }

      final Finder deviceNameFinder = find.text(deviceName);
      expect(deviceNameFinder, findsOneWidget);
    });
  });
}
