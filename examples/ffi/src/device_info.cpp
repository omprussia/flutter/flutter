/**
 * SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <cstring>

#include <QtDBus/QtDBus>

#include "device_info.h"

namespace {

const char* copyString(const QString& str) {
    auto* tmp = new char[str.length() + 1];
    std::strcpy(tmp, str.toUtf8().data());
    return tmp;
}

}  // namespace

const char* getDeviceName() {
    if (QDBusConnection::systemBus().isConnected()) {
        QDBusInterface iface("ru.omp.deviceinfo", "/ru/omp/deviceinfo/Features", "ru.omp.deviceinfo.Features",
                             QDBusConnection::systemBus());
        if (iface.isValid()) {
            QDBusReply<QString> reply = iface.call("getDeviceModel");
            if (reply.isValid()) {
                return copyString(reply.value());
            }
        }
    }
    return "";
}
