// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:safearea_mediaquery/main.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('| Integration tests for SafeArea and MediaQuery application |', () {
    testWidgets('**Check app startup and UI display**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder appNameFinder = find.text('MediaQuery and SafeArea demo');
      expect(appNameFinder, findsOneWidget);
    });

    testWidgets('**Check the display of both containers**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      // Find the container without SafeArea by color
      final Finder containerWithoutSafeAreaFinder = find.byWidgetPredicate(
        (Widget widget) => widget is Container && (widget.color == InternalColors.red),
      );

      // Find the container with SafeArea by color
      final Finder containerWithSafeAreaFinder = find.byWidgetPredicate(
        (Widget widget) => widget is Container && (widget.color == InternalColors.midnight),
      );

      expect(containerWithoutSafeAreaFinder, findsOneWidget);
      expect(containerWithSafeAreaFinder, findsOneWidget);
    });

    testWidgets('**Check the display of MediaQuery values**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder safeAreaFinder = find.byType(SafeArea);
      expect(safeAreaFinder, findsOneWidget);

      final BuildContext context = tester.element(safeAreaFinder);

      final MediaQueryData mediaQueryData = MediaQuery.of(context);

      // Check the display of padding values
      final Finder topPaddingFinder = find.byKey(const Key('paddingTop'));
      expect(topPaddingFinder, findsOneWidget);
      expect(
        find.descendant(
          of: topPaddingFinder,
          matching: find.text(mediaQueryData.padding.top.toString()),
        ),
        findsOneWidget,
      );

      final Finder bottomPaddingFinder = find.byKey(const Key('paddingBottom'));
      expect(bottomPaddingFinder, findsOneWidget);
      expect(
        find.descendant(
          of: bottomPaddingFinder,
          matching: find.text(mediaQueryData.padding.bottom.toString()),
        ),
        findsOneWidget,
      );

      final Finder leftPaddingFinder = find.byKey(const Key('paddingLeft'));
      expect(leftPaddingFinder, findsOneWidget);
      expect(
        find.descendant(
          of: leftPaddingFinder,
          matching: find.text(mediaQueryData.padding.left.toString()),
        ),
        findsOneWidget,
      );

      final Finder rightPaddingFinder = find.byKey(const Key('paddingRight'));
      expect(rightPaddingFinder, findsOneWidget);
      expect(
        find.descendant(
          of: rightPaddingFinder,
          matching: find.text(mediaQueryData.padding.right.toString()),
        ),
        findsOneWidget,
      );
    });

    testWidgets('**Check the behavior of TextFormField when the keyboard appears**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder textFormFieldFinder = find.byType(TextFormField);
      expect(textFormFieldFinder, findsOneWidget);

      await tester.tap(textFormFieldFinder);
      await tester.pumpAndSettle();

      // Check that the keyboard appeared and TextFormField is visible
      expect(WidgetsBinding.instance.window.viewInsets.bottom != 0, isTrue);
    });

    testWidgets('**Scrolling ListView hides the keyboard**', (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder textFormFieldFinder = find.byType(TextFormField);
      await tester.tap(textFormFieldFinder);
      await tester.pumpAndSettle();

      // Ensure that the keyboard is open
      expect(WidgetsBinding.instance.window.viewInsets.bottom != 0, isTrue);

      // Scroll the list
      await tester.drag(find.byType(ListView), const Offset(0, -100));
      await tester.pumpAndSettle();

      await tester.pump(const Duration(seconds: 2));

      // Check that the keyboard is hidden
      expect(WidgetsBinding.instance.window.viewInsets.bottom != 0, isFalse);
    });
  });
}
