// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/material.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';

void main() {
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: const Scaffold(
        body: Stack(children: <Widget>[
          _ContainerWithoutSafeArea(),
          _ContainerWithSafeArea(),
        ]),
      ),
    );
  }
}

class _ContainerWithoutSafeArea extends StatelessWidget {
  const _ContainerWithoutSafeArea();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: InternalColors.red,
    );
  }
}

class _ContainerWithSafeArea extends StatelessWidget {
  const _ContainerWithSafeArea();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 16,
        ),
        color: InternalColors.midnight,
        child: ListView(
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          children: <Widget>[
            const SizedBox(height: 8),
            const Text(
              'MediaQuery and SafeArea demo',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 20),
            const Text(
              'This demo shows the SafeArea application areas and also displays the MediaQuery indents on the screen',
              style: TextStyle(
                fontSize: 15,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 16),
            ListItemData<String>(
              'MediaQuery padding top',
              description: 'Display padding top for MediaQuery',
              InternalColors.pink,
              key: const ValueKey<String>('paddingTop'),
              value: MediaQuery.of(context).padding.top.toString(),
            ),
            const SizedBox(height: 8),
            ListItemData<String>(
              'MediaQuery padding bottom',
              description: 'Display padding bottom for MediaQuery',
              InternalColors.green,
              key: const ValueKey<String>('paddingBottom'),
              value: MediaQuery.of(context).padding.bottom.toString(),
            ),
            const SizedBox(height: 8),
            ListItemData<String>(
              'MediaQuery padding left',
              description: 'Display padding left for MediaQuery',
              InternalColors.orange,
              key: const ValueKey<String>('paddingLeft'),
              value: MediaQuery.of(context).padding.left.toString(),
            ),
            const SizedBox(height: 8),
            ListItemData<String>(
              'MediaQuery padding right',
              description: 'Display padding right for MediaQuery',
              InternalColors.coal,
              key: const ValueKey<String>('paddingRight'),
              value: MediaQuery.of(context).padding.right.toString(),
            ),
            TextFormField(
              scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
    );
  }
}
