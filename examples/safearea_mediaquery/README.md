# SafeArea & MediaQuery

Demonstrates how to use the SafeArea & MediaQuery on Aurora OS.

You can read more about
[SafeArea & MediaQuery](https://docs.flutter.dev/ui/adaptive-responsive/safearea-mediaquery).

## Aurora OS

You can use the commands `flutter build` and `flutter run` from the app's root
directory to build/run the app.
