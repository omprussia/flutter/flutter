// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'custom_text_field.dart';

class CustomKeyboardListenerInput extends StatefulWidget {
  const CustomKeyboardListenerInput({
    super.key,
    this.textFiledHeight,
  });

  final double? textFiledHeight;

  @override
  State<CustomKeyboardListenerInput> createState() =>
      _CustomKeyboardListenerInputState();
}

class _CustomKeyboardListenerInputState
    extends State<CustomKeyboardListenerInput> {
  Color _decorationColor = Colors.grey;
  TextEditingValue _textEditingValue = const TextEditingValue(
      text: 'initial text', selection: TextSelection.collapsed(offset: 12));
  bool _showCursor = false;
  final FocusNode _focusNode =
      FocusNode(debugLabel: 'Custom keyboard listener input field');

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: _onTap,
      child: Focus(
        focusNode: _focusNode,
        onFocusChange: _onFocusChange,
        // This call may throws error: 'std::out_of_range' or ignore key events
        onKeyEvent: _onKeyEvent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: CustomTextField(
                selection: _textEditingValue.selection,
                text: _textEditingValue.text,
                showCursor: _showCursor,
              ),
            ),
            Divider(
              thickness: 2.0,
              color: _decorationColor,
            ),
          ],
        ),
      ),
    );
  }

  void _onTap() {
    if (!_focusNode.hasFocus) {
      _focusNode.requestFocus();
    }
  }

  void _onFocusChange(bool focused) {
    if (focused) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => SystemChannels.textInput.invokeMethod('TextInput.show'),
      );
    } else {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    }
    setState(() {
      _decorationColor = focused ? Colors.blue : Colors.grey;
      _showCursor = focused;
    });
  }

  KeyEventResult _onKeyEvent(FocusNode _, KeyEvent event) {
    if (event case final KeyDownEvent keyDownEvent) {
      if (keyDownEvent.logicalKey == LogicalKeyboardKey.backspace) {
        final String text = _textEditingValue.text;
        setState(() {
          _textEditingValue = _textEditingValue.copyWith(
              text: text.isEmpty ? '' : text.substring(0, text.length - 1));
        });
        return KeyEventResult.handled;
      }
      if (keyDownEvent.logicalKey == LogicalKeyboardKey.enter) {
        _focusNode.unfocus();
        return KeyEventResult.handled;
      }
      if (keyDownEvent.character?.isNotEmpty ?? false) {
        final String text = _textEditingValue.text;
        setState(() {
          _textEditingValue = _textEditingValue.copyWith(
              text: text + keyDownEvent.character!,
              selection: TextSelection.collapsed(offset: text.length + 1));
        });
        return KeyEventResult.handled;
      }
    }
    return KeyEventResult.ignored;
  }
}
