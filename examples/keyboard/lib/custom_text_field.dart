// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'dart:async';

import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    super.key,
    required this.selection,
    required this.showCursor,
    required this.text,
    this.textFieldHeight = 24.0,
  });

  final double textFieldHeight;
  final String text;
  final TextSelection selection;
  final bool showCursor;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  Timer? _cursorTimer;
  late bool _showCursor;

  @override
  void initState() {
    super.initState();
    _showCursor = widget.showCursor;
  }

  @override
  void dispose() {
    _cursorTimer?.cancel();
    _cursorTimer = null;
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant CustomTextField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.showCursor) {
      _startCursorTimer();
    } else {
      _stopCursorTimer();
    }
  }

  void _startCursorTimer() {
    // If timer has already started, no need to start it again
    if (_cursorTimer != null) {
      if (_cursorTimer!.isActive) {
        return;
      }
      _cursorTimer?.cancel();
    }
    setState(() {
      _showCursor = true;
    });
    _cursorTimer = Timer.periodic(
      const Duration(milliseconds: 500),
      _toggleShowCursor,
    );
  }

  void _stopCursorTimer() {
    setState(() {
      _showCursor = false;
    });
    _cursorTimer?.cancel();
  }

  void _toggleShowCursor(Timer timer) {
    setState(() {
      _showCursor = !_showCursor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: CustomPaint(
        size: Size.fromHeight(widget.textFieldHeight),
        painter: _TextFieldPainter(
          text: widget.text,
          selection: widget.selection,
          showCursor: _showCursor,
          style: Theme.of(context).textTheme.bodyLarge!,
        ),
      ),
    );
  }
}

class _TextFieldPainter extends CustomPainter {
  const _TextFieldPainter({
    required this.text,
    required this.selection,
    required this.showCursor,
    this.style = const TextStyle(
      color: Colors.black,
      fontSize: 18.0,
    ),
  });
  final String text;
  final TextSelection selection;
  final bool showCursor;
  final TextStyle style;

  @override
  void paint(Canvas canvas, Size size) {
    final TextSpan textSpan = TextSpan(
      text: text,
      mouseCursor: MouseCursor.defer,
      style: style,
    );
    final TextPainter textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );

    textPainter.layout();

    textPainter.paint(canvas, Offset.zero);

    if (showCursor) {
      final Offset caretOffset = textPainter
          .getOffsetForCaret(selection.extent, Rect.zero)
          .translate(2.0, 0.0);
      final Paint paint = Paint()
        ..color = Colors.blue
        ..strokeWidth = 2.0;

      canvas.drawLine(
        caretOffset,
        caretOffset.translate(0, textPainter.height),
        paint,
      );
    }
  }

  @override
  bool shouldRepaint(_TextFieldPainter oldDelegate) {
    return text != oldDelegate.text || showCursor != oldDelegate.showCursor;
  }
}
