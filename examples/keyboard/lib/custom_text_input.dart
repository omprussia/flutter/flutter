// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'custom_text_field.dart';

class CustomTextInput extends StatefulWidget {
  const CustomTextInput({
    super.key,
    this.textFiledHeight,
  });

  final double? textFiledHeight;

  @override
  State<CustomTextInput> createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput>
    implements TextInputClient {
  TextInputConnection? _textInputConnection;
  TextEditingValue _textEditingValue = const TextEditingValue(
      text: 'initial text', selection: TextSelection.collapsed(offset: 12));
  final FocusNode _focusNode = FocusNode(debugLabel: 'Custom text input field');

  bool _showCursor = false;
  Color _decorationColor = Colors.grey;

  @override
  void dispose() {
    _closeConnection();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: _onTap,
      child: Focus(
        focusNode: _focusNode,
        onFocusChange: _onFocusChange,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: CustomTextField(
                selection: _textEditingValue.selection,
                text: _textEditingValue.text,
                showCursor: _showCursor,
              ),
            ),
            Divider(
              thickness: 2.0,
              color: _decorationColor,
            ),
          ],
        ),
      ),
    );
  }

  void _onTap() {
    if (!_focusNode.hasFocus) {
      _focusNode.requestFocus();
    }
  }

  void _onFocusChange(bool focused) {
    if (focused) {
      _openConnection();
    } else {
      _closeConnection();
    }
    setState(() {
      _decorationColor = focused ? Colors.blue : Colors.grey;
      _showCursor = focused;
    });
  }

  void _openConnection() {
    _textInputConnection ??=
        TextInput.attach(this, const TextInputConfiguration());
    _textInputConnection?.setEditingState(_textEditingValue);
    _textInputConnection?.show();
  }

  void _closeConnection() {
    _textInputConnection?.close();
    _textInputConnection = null;
  }

  @override
  void updateEditingValue(TextEditingValue value) {
    setState(() {
      _textEditingValue = value;
    });
  }

  @override
  void performAction(TextInputAction action) {
    if (action == TextInputAction.done) {
      _focusNode.unfocus();
    }
  }

  @override
  TextEditingValue get currentTextEditingValue => _textEditingValue;

  @override
  AutofillScope? get currentAutofillScope => null;

  @override
  void connectionClosed() {
    _focusNode.unfocus();
  }

  @override
  void didChangeInputControl(
      TextInputControl? oldControl, TextInputControl? newControl) {
    // TODO(n.krasavin): implement didChangeInputControl
    if (kDebugMode) {
      print('didChangeInputControl');
    }
  }

  @override
  void insertContent(KeyboardInsertedContent content) {
    // TODO(n.krasavin): implement insertContent
    if (kDebugMode) {
      print('insertContent');
    }
  }

  @override
  void insertTextPlaceholder(Size size) {
    // TODO(n.krasavin): implement insertTextPlaceholder
    if (kDebugMode) {
      print('insertTextPlaceholder');
    }
  }

  @override
  void performPrivateCommand(String action, Map<String, dynamic> data) {
    // TODO(n.krasavin): implement performPrivateCommand
    if (kDebugMode) {
      print('performPrivateCommand');
    }
  }

  @override
  void performSelector(String selectorName) {
    // TODO(n.krasavin): implement performSelector
    if (kDebugMode) {
      print('performSelector');
    }
  }

  @override
  void removeTextPlaceholder() {
    // TODO(n.krasavin): implement removeTextPlaceholder
    if (kDebugMode) {
      print('removeTextPlaceholder');
    }
  }

  @override
  void showAutocorrectionPromptRect(int start, int end) {
    // TODO(n.krasavin): implement showAutocorrectionPromptRect
    if (kDebugMode) {
      print('didChangeInputControl');
    }
  }

  @override
  void showToolbar() {
    // TODO(n.krasavin): implement showToolbar
    if (kDebugMode) {
      print('showToolbar');
    }
  }

  @override
  void updateFloatingCursor(RawFloatingCursorPoint point) {
    // TODO(n.krasavin): implement updateFloatingCursor
    if (kDebugMode) {
      print('updateFloatingCursor');
    }
  }
}
