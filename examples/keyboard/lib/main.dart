// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import 'package:flutter/material.dart';
import 'custom_keyboard_listener_input.dart';
import 'custom_text_input.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Keyboard',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Aurora Keyboard Demo'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: FocusScope(
            child: Column(
              children: <Widget>[
                const Text(
                  'Flutter TextField widget 1:',
                  textAlign: TextAlign.start,
                ),
                const SizedBox(height: 8),
                const TextField(),
                const SizedBox(height: 24),
                const Text(
                  'Custom keyboard listener widget:',
                  textAlign: TextAlign.start,
                ),
                const SizedBox(height: 8),
                const CustomKeyboardListenerInput(),
                const Text(
                  'Custom text input widget:',
                  textAlign: TextAlign.start,
                ),
                const SizedBox(height: 8),
                const CustomTextInput(),
                const SizedBox(height: 24),
                ElevatedButton(
                  onPressed: FocusScope.of(context).unfocus,
                  child: Text('UNFOCUS', style: Theme.of(context).textTheme.titleMedium),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
