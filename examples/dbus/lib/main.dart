// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:dbus/dbus.dart';
import 'package:flutter/material.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';

import 'ru_omp_deviceinfo_Features.dart';

class Dbus {
  /// Implementation of a method for obtaining a device model.
  Future<String?> getDeviceName() async {
    // Initialization of the D-Bus client
    final DBusClient client = DBusClient.system();

    // Initializing an object
    final RuOmpDeviceinfoFeatures features = RuOmpDeviceinfoFeatures(
      client,
      'ru.omp.deviceinfo',
      DBusObjectPath('/ru/omp/deviceinfo/Features'),
    );

    // Method Execution
    final String deviceName = await features.callgetDeviceModel();

    // Closing the D-Bus client
    await client.close();

    // Returning the result
    return deviceName == '' ? null : deviceName;
  }
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Dbus _plugin = Dbus();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: internalTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'D-Bus',
            key: ValueKey<String>('appBarTitle'),
          ),
        ),
        body: ListSeparated(
          children: <Widget>[
            const ListItemInfo('''
            An example of a platform-dependent plugin that works with the
            Aurora OS via the D-Bus interface.
            '''),
            ListItemData<String?>(
              'Get Device Name',
              InternalColors.purple,
              description:
                  'Getting the device name through a platform-dependent plugin on D-Bus',
              future: _plugin.getDeviceName(),
            ),
          ],
        ),
      ),
    );
  }
}
