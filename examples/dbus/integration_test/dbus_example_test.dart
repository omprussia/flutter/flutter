// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>

import 'package:dbus_example/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('| D-Bus Example Integration Tests |', () {
    testWidgets('**Check app startup and UI display**',
        (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final Finder appBarFinder = find.byKey(const Key('appBarTitle'));
      expect(appBarFinder, findsOneWidget);
    });

    testWidgets('**Test D-Bus getDevice method**', (WidgetTester tester) async {
      final Dbus plugin = Dbus();

      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      final String? deviceName = await plugin.getDeviceName();
      if (deviceName == null) {
        fail('D-BUS getDeviceName method failed');
      }

      final Finder deviceNameFinder = find.text(deviceName);
      expect(deviceNameFinder, findsOneWidget);
    });
  });
}
