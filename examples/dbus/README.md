# D-Bus

Demonstrates how to use the D-Bus on Aurora OS.

You can read more about
[D-Bus](https://www.freedesktop.org/wiki/Software/dbus/).

## Aurora OS

You can use the commands `flutter build` and `flutter run` from the app's root
directory to build/run the app.

Activate dbus:
`dart pub global activate dbus`
and generate bindings:
`dart-dbus generate-remote-object ru.omp.deviceinfo.Features.xml -o lib/ru_omp_deviceinfo_Features.dart`.
