## Updated: 12/24/2024 15:45:00 AM

## Info

- Last tag: 3.27.1
- Released: 7

## Versions

- Version: 3.27.1 (24/12/2024)
- Version: 3.24.0 (03/09/2024)
- Version: 3.16.2-2 (22/04/2024)
- Version: 3.16.2-1 (28/12/2023)
- Version: 3.16.2 (26/12/2023)
- Version: 3.13.5 (26/12/2023)
- Version: 3.3.10 (26/12/2023)

### Version: 3.27.1 (24/12/2024)

#### Bug

- Verify the directory for saving screenshots.
- Remove package installation via pkcon.

#### Change

- Remove outdated documentation and update the documentation link.
- Keyboard example: Add unfocus() in the connectionClosed() callback.
- Add a caret for text fields in the keyboard example.
- Update the wakelock_plus plugin version.
- Remove USB device check by IP.
- Update CLI text messages.
- Relax sudoers validation.
- Update examples documentation and installation instructions.
- Update .gitignore for examples.
- Update .gitignore templates.
- Revise documentation for the 3.24.0 release.
- Fix broken links in Aurora OS articles.

#### Feature

- Add integration tests for safearea_mediaquery.
- Add integration tests for platform_channels_qt.
- Add integration tests for platform_channel.
- Add integration tests for ffi.
- Add integration tests for dbus.
- Add integration tests for client_wrapper.
- Add a Dart entry point example app.
- Implement RawKeyEvent for Aurora OS.
- Add an example package for platform_channel.
- Add an example package for SafeArea and MediaQuery.
- Add an example package for lifecycle management.
- Add an example package for dbus.
- Add an example package for ffi.
- Add an example package for client_wrapper.
- Add an example package for platform_channels_qt.

### Version: 3.24.0 (03/09/2024)

#### Bug

- Fix build argument 'psdk-dir'.
- Fix documentation.
- Download engine for 'main' branch by branch name.
- Update doctor - checking embedder installation.

#### Change

- Update documentation for 3.24.0 release.
- Fix links in Aurora OS articles.
- Apply patch platform aurora.
- Refactoring and correcting documentation errors.
- Update plugins documentation.
- Update platform_channels_qt demo information.
- Add old variant install via apm.
- Update doctor and PSDK.
- Remove print info about state progress.
- Add remove aurora keys from config.
- Corrected spelling, punctuation and semantic errors.

#### Feature

- Add support Flutter CLI assemble.
- Add support Flutter CLI drive.
- Add support Flutter cli logs.
- Add support Flutter cli screenshots emulator.
- Add support Flutter cli screenshots device.
- Add configuration sudoers before update aurora-psdk-dir.
- Add support Flutter CLI install.
- Add support Flutter CLI attach.
- Fix devices for integration_test.
- Add support for devices and emulator.
- Add targets for custom devices aurora.
- Flutter CLI. Emulators search & run.
- Adding channels, updating via artifacts, precache command.
- Add to documentation about format Dart and C++ code.
- Add check Platform SDK targets.

### Version: 3.16.2-2 (22/04/2024)

#### Bug

- Update doctor - checking embedder installation.

#### Change

- Disable repositories in check version flutter embedder.
- Updated documentation with client wrapper.
- Update template plugin platform channels.
- Remove dependency flutter-embedder.
- Add --offline flag check with has version.
- Update template for method RegisterPlugins.
- Fix typos and broken links in documentation.
- Update install embedder.
- Block version downgrade.
- Add Flutter CLI upgrade & downgrade.
- Add sort install rpm embedder.
- Update check targets name AuroraOS -> Aurora

#### Feature

- Add flag --offline for aurora build.
- Add snapcraft configuration.
- Add auto install embedder to platform sdk.
- Add documentation plugin template readme.md.
- Add documentation about gitflow.
- Add documentation about mkdocs.
- Add documentation about platform texture.
- Add documentation about platform methods.

### Version: 3.16.2-1 (28/12/2023)

#### Change

- Download engine by latest tag version Flutter SDK.

#### Feature

- Add changeln config and templates.
- Add docs for static web-site gitlab pages.
- Add architecture for Platform SDK 5+

### Version: 3.16.2 (26/12/2023)

#### Change

- Flutter SDK update to 3.16.2.

### Version: 3.13.5 (26/12/2023)

#### Change

- Flutter SDK update to 3.13.5.

### Version: 3.3.10 (26/12/2023)

#### Feature

- Add Flutter SDK 3.3.10 with target for Aurora OS.
